import React, { useContext } from "react";
import { RootStoreContext } from "../../app/stores/rootStore";
import { Segment } from "semantic-ui-react";
import PhotoUploadWidget from "../../app/common/photoUpload/PhotoUploadWidget";
import { observer } from "mobx-react-lite";

const ServiceFormImage: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { uploadingPhoto, uploadPhoto } = rootStore.serviceStore;

  return (
    <Segment loading={uploadingPhoto}>
      <PhotoUploadWidget loading={uploadingPhoto} uploadPhoto={uploadPhoto} />
    </Segment>
  );
};

export default observer(ServiceFormImage);
