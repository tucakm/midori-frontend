import { Moment } from "moment";
import { IOrder } from "./order";
import { IService } from "./service";
import { IUser } from "./user";

export interface INotification {
  id: string;
  orderedService: IService;
  user: IUser;
  date: Moment;
  isRead: boolean;
  message: string;
  type: NotificationType;
  order: IOrder;
}

export enum NotificationType {
  NewOrder = 1,
  RescheduleOrder = 2,
  Review = 3,
}
