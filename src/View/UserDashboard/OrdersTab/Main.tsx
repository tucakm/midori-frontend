import { Button, Col, Row, Table } from "antd";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { currencyFormater } from "../../../app/common/currency/CurrencyInput";
import { DateDisplay } from "../../../app/common/date-display/DateDisplay";
import { IOrder, OrderStatus } from "../../../app/models/order";
import { RootStoreContext } from "../../../app/stores/rootStore";
import { getTagByOrderStatus } from "../CalendarTab/OrderInfo";

const Main: React.FC = () => {
  const [dataSource, setDataSource] = useState<IOrder[]>();
  const rootStore = useContext(RootStoreContext);
  const { loadSentOrders, loadingOrders, changeStatus } = rootStore.orderStore;
  const { loadNotifications } = rootStore.notificationStore;
  const { t } = useTranslation();
  const handleChangeOfStatus = (status: OrderStatus, order: IOrder) => {
    changeStatus(order.id, status).then(() =>
      loadSentOrders().then((resposne) => setDataSource(resposne)).finally(() => loadNotifications())
    );
  };

  const columns = [
    {
      title: t("OrderTab.ServiceColum"),
      dataIndex: ["service", "name"],
      key: "service.name",
    },
    {
      title: t("OrderTab.DateColumn"),
      dataIndex: "date",
      key: "date",
      render: (item) => <DateDisplay format="DD/MM/YYYY" value={item} />,
    },
    {
      title: t("OrderTab.StartTime"),
      dataIndex: "startTime",
      key: "startTime",
      render: (item) => <DateDisplay format="HH" value={item} />,
    },
    {
      title: t("OrderTab.EndTime"),
      dataIndex: "endTime",
      key: "endTime",
      render: (item) => <DateDisplay format="HH" value={item} />,
    },
    {
      title: t("OrderTab.TotalPrice"),
      dataIndex: "totalPrice",
      key: "totalPrice",
      render: (item) => currencyFormater(item),
    },
    {
      title: t("OrderTab.Status"),
      dataIndex: "status",
      key: "status",
      render: (item) =>
        item !== OrderStatus.RescheduleNeeded
          ? getTagByOrderStatus(item)
          : getTagByOrderStatus(OrderStatus.Pending),
    },
    {
      title: t("OrderTab.Action"),
      key: "action",
      render: (text, record) => (
        <>
          {record.status === OrderStatus.RescheduleSent && (
            <Row gutter={16}>
              <Col flex="auto">
                <Button
                  block
                  type="primary"
                  onClick={() =>
                    handleChangeOfStatus(OrderStatus.Aproved, record)
                  }
                >
                  {t("OrderTab.ActionAccept")}
                </Button>
              </Col>
              <Col flex="auto">
                <Button
                  block
                  danger
                  onClick={() =>
                    handleChangeOfStatus(OrderStatus.Declined, record)
                  }
                >
                  {t("OrderTab.ActionDecline")}
                </Button>
              </Col>
            </Row>
          )}
        </>
      ),
    },
  ];
  useEffect(() => {
    loadSentOrders().then((response) => setDataSource(response));
  }, [loadSentOrders]);
  return (
    <div className="container-fluid card">
      <div className="p-20">
        <Table
          dataSource={dataSource}
          loading={loadingOrders}
          columns={columns}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => console.log(record),
            };
          }}
        />
      </div>
    </div>
  );
};

export default observer(Main);
