import { Col, Row, Image as AntImage, Typography, Tag } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";
import { useTranslation } from "react-i18next";
import { ITag } from "../../../app/models/tag";
import { useServiceFormContext } from "../../../app/util/hooks";

const Summary = () => {
  const serviceContext = useServiceFormContext();
  const { serviceModel } = serviceContext;
  const { t } = useTranslation();
  return (
    <div className="container">
      <Row gutter={16}>
        <Col span={8}>
          <AntImage
            src={serviceModel.image ?? "/assets/imageplaceholder.png"}
          />
        </Col>
        <Col span={16}>
          <Row style={{ marginBottom: 16 }}>
            <Col span={24}>
              <h5>{serviceModel.name}</h5>
            </Col>
          </Row>
          <Row style={{ marginBottom: 8 }}>
            <Col span={24}>
              <label>
                <Typography.Text type="secondary">
                  {t("ServiceForm.LabelIntroduction")}
                </Typography.Text>
              </label>
            </Col>
            <Col span={24}>
              <span>{serviceModel.introduction}</span>
            </Col>
          </Row>
          <Row style={{ marginBottom: 8 }}>
            <Col span={24}>
              <label>
                <Typography.Text type="secondary">
                  {t("ServiceForm.LabelDescription")}
                </Typography.Text>
              </label>
            </Col>
            <Col span={24}>
              <span>{serviceModel.description}</span>
            </Col>
          </Row>
          <Row style={{ marginBottom: 8 }}>
            <Col span={24}>
              <label>
                <Typography.Text type="secondary">
                  {t("ServiceForm.LabelPrice")}
                </Typography.Text>
              </label>
            </Col>
            <Col span={24}>
              <span>
                <strong>{serviceModel.price} HRK</strong>
              </span>
            </Col>
          </Row>
          <Row style={{ marginBottom: 8 }}>
            <Col span={24}>
              <label>
                <Typography.Text type="secondary">
                  {t("ServiceForm.City")}
                </Typography.Text>
              </label>
            </Col>
            <Col span={24}>
              <span>{serviceModel.city}</span>
            </Col>
          </Row>
          <Row style={{ marginBottom: 8 }}>
            <Col span={24}>
              <label>
                <Typography.Text type="secondary">
                  {t("ServiceForm.LabelTags")}
                </Typography.Text>
              </label>
            </Col>
            <Col span={24}>
              {serviceModel.serviceTags &&
                serviceModel.serviceTags.map((tag: ITag) => {
                  return <Tag>{tag.tagName}</Tag>;
                })}
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default observer(Summary);
