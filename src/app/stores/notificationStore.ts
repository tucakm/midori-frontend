import { action, computed, observable, runInAction } from "mobx";
import agent from "../api/agent";
import { INotification } from "../models/notification";
import { RootStore } from "./rootStore";

export default class NotificationStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @observable loadingNotifications = false;
  @observable notification: INotification | null = null;
  @observable notificationRegisty = new Map();

  @computed get ordersArray() {
    return Array.from(this.notificationRegisty.values());
  }
  @action loadNotifications = async () => {
    this.loadingNotifications = true;
    try {
      const notif = await agent.Notifications.listForUser(
        this.rootStore.userStore.user.username
      );
      runInAction(() => {
        notif.forEach((notific) => {
          this.notificationRegisty.set(notific.id, notific);
        });
        this.loadingNotifications = false;
      });
    } catch (error) {
      runInAction(() => {
        console.log(error);
        this.loadingNotifications = false;
      });
    }
  };
  @action toggleIsRead = async (notification: INotification) => {
    try {
      await agent.Notifications.toggleIsRead(notification.id, true);
      runInAction(() => {
        this.notificationRegisty.delete(notification.id);
        this.loadingNotifications = false;
      });
    } catch (error) {
      runInAction(() => {
        console.log(error);
        this.loadingNotifications = false;
      });
    }
  };
}
