import React, { Fragment, useState, useEffect, useContext } from "react";
import { Grid, List, ChevronDown } from "react-feather";
import errorImg from "../../assets/images/search-not-found.png";
import { RootStoreContext } from "../../app/stores/rootStore";
import { LoadingComponent } from "../../app/layout/LoadingComponent";
import { observer } from "mobx-react-lite";
import { IService } from "../../app/models/service";
import { RouteComponentProps } from "react-router-dom";
import ServiceItemV2 from "./ServiceItemV2";
import InfiniteScroll from "react-infinite-scroll-component";
import { useTranslation } from "react-i18next";
import { useCallback } from "react";



const ServiceDashboardV2: React.FC<RouteComponentProps> = ({ location }) => {
  const layoutColumns = 3;
  const [filterSidebar, setFilterSidebar] = useState(true);
  const rootStore = useContext(RootStoreContext);
  const [filteredServices, setFilteredServices] = useState<IService[]>();
  const {
    loadServices,
    loadingInitial,
    filterServices,
    setPage,
    page,
    totalPage,
    serviceCount,
    sorting
  } = rootStore.serviceStore;
  const [loadingNext, setLoadingNext] = useState<boolean>(false);
  const searchParams = new URLSearchParams(location.search);
  const search = searchParams.get("criteria") || "";
  const { t } = useTranslation();
  const [sort, setSort] = useState<string>();

  const gridLayout = () => {
    if (filteredServices && filteredServices.length > 0) {
      document
        .querySelector(".product-wrapper-grid")!
        .classList.remove("list-view");
      var elems = document.querySelector(".gridRow")!.childNodes;
      [].forEach.call(elems, function (el: HTMLElement) {
        el.className = "";
        el.classList.add("col-xl-3");
        el.classList.add("col-sm-6");
        el.classList.add("xl-4");
      });
    }
  };

  //List Layout View
  const listLayout = () => {
    if (filteredServices && filteredServices.length > 0) {
      document.querySelector(".product-wrapper-grid")!.classList.add("list-view");
      var elems = document.querySelector(".gridRow")!.childNodes;
      [].forEach.call(elems, function (el: HTMLElement) {
        el.className = "";
        el.classList.add("col-xl-12");
      });
    }
  };

  // Layout Column View
  const LayoutView = (layoutColumns: number) => {
    if (filteredServices && filteredServices.length > 0) {
      if (
        !document
          .querySelector(".product-wrapper-grid")!
          .classList.contains("list-view")
      ) {
        var elems = document.querySelector(".gridRow")!.childNodes;
        [].forEach.call(elems, function (el: HTMLElement) {
          el.className = "";
          el.classList.add("col-xl-" + layoutColumns);
        });
      }
    }
  };

  const filterSortFunc = useCallback((event: string) => {
    setPage(0);
    loadServices(search, event)
      .then(() =>
        setFilteredServices(filterServices(search)))
      .then(() => setLoadingNext(false))


    setSort(event);
  }, [loadServices, search, setPage, filterServices]);


  useEffect(() => {
    if (search) {
      loadServices(search).then(() => setFilteredServices(filterServices(search)));
    }
    return setPage(0)
  }, [loadServices, search, filterServices, setPage]);

  const handleGetNext = () => {
    setLoadingNext(true);
    setPage(page + 1);
    loadServices(search, sort)
      .then(() => setFilteredServices(filterServices(search)))
      .then(() => setLoadingNext(false));
  };
  if (loadingInitial && page === 0)
    return <LoadingComponent content={t("LoadingIndicators.LoadingServices")} />;
  return (
    <Fragment>
      <div className="container-fluid product-wrapper" style={{ paddingTop: "77px" }}>
        <div className="product-grid">
          <div className="feature-products">
            <div className="row">
              <div className="col-md-6 products-total">
                <div className="square-product-setting d-inline-block">
                  <div
                    className="icon-grid grid-layout-view "
                    onClick={gridLayout}
                    data-original-title=""

                  >
                    <Grid />
                  </div>
                </div>
                <div className="square-product-setting d-inline-block">
                  <div
                    className="icon-grid m-0 list-layout-view"
                    onClick={listLayout}
                    data-original-title=""
                    title=""

                  >
                    <List />
                  </div>
                </div>
                <span
                  className="d-none-productlist filter-toggle"
                  onClick={() => setFilterSidebar(!filterSidebar)}
                >
                  <h6 className="mb-0">
                    Filters
                    <span className="ml-2">
                      <ChevronDown className="toggle-data" />
                    </span>
                  </h6>
                </span>
                <div className="grid-options d-inline-block">
                  <ul>
                    <li>
                      <a
                        className="product-2-layout-view"
                        onClick={() => LayoutView(6)}
                        data-original-title=""
                        title=""
                      >
                        <span className="line-grid line-grid-1 bg-primary"></span>
                        <span className="line-grid line-grid-2 bg-primary"></span>
                      </a>
                    </li>
                    <li>
                      <a
                        className="product-3-layout-view"
                        onClick={() => LayoutView(4)}
                        data-original-title=""
                        title=""
                      >
                        <span className="line-grid line-grid-3 bg-primary"></span>
                        <span className="line-grid line-grid-4 bg-primary"></span>
                        <span className="line-grid line-grid-5 bg-primary"></span>
                      </a>
                    </li>
                    <li>
                      <a
                        className="product-4-layout-view"
                        onClick={() => LayoutView(3)}
                        data-original-title=""
                        title=""
                      >
                        <span className="line-grid line-grid-6 bg-primary"></span>
                        <span className="line-grid line-grid-7 bg-primary"></span>
                        <span className="line-grid line-grid-8 bg-primary"></span>
                        <span className="line-grid line-grid-9 bg-primary"></span>
                      </a>
                    </li>
                    <li>
                      <a
                        className="product-6-layout-view"
                        onClick={() => LayoutView(2)}
                        data-original-title=""
                        title=""
                      >
                        <span className="line-grid line-grid-10 bg-primary"></span>
                        <span className="line-grid line-grid-11 bg-primary"></span>
                        <span className="line-grid line-grid-12 bg-primary"></span>
                        <span className="line-grid line-grid-13 bg-primary"></span>
                        <span className="line-grid line-grid-14 bg-primary"></span>
                        <span className="line-grid line-grid-15 bg-primary"></span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 text-right">
                <span className="f-w-600 m-r-10">
                  {filteredServices === undefined ||
                    filteredServices?.length === 0
                    ? t("ServiceDashboard.NoResultsFilter")
                    : t("ServiceDashboard.FilterCount", { count: filteredServices.length, totalCount: serviceCount })}
                </span>
                <div className="select2-drpdwn-product select-options d-inline-block">
                  <select
                    className="form-control btn-square"
                    onChange={(e) => filterSortFunc(e.target.value)}
                  >
                    <option value="">
                      {t("ServiceDashboard.SortingItems")}
                    </option>
                    <option value="DESC">
                      {t("ServiceDashboard.PriceHL")}
                    </option>
                    <option value="ASC">
                      {t("ServiceDashboard.PriceLH")}
                    </option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="product-wrapper-grid" >
            {sorting && (
              <LoadingComponent content={t("LoadingIndicators.LoadingServices")} />)
            }
            {(filteredServices === undefined ||
              filteredServices.length === 0) && (!loadingInitial && !sorting) ? (
              <div className="search-not-found text-center">
                <div>
                  <img
                    className="img-fluid second-search"
                    src={errorImg}
                    alt=""
                  />
                  <p>{t("ServiceDashboard.DidntFindAny")}</p>
                </div>
              </div>
            ) : (
              <InfiniteScroll
                dataLength={filteredServices.length}
                next={handleGetNext}
                hasMore={!loadingNext && page + 1 < totalPage}
                loader={null}
                style={{ overflowX: "hidden" }}
              >
                <div className="row gridRow">
                  {filteredServices.map((item, i) => (
                    <div
                      className={`${layoutColumns === 3
                        ? "col-xl-3 col-sm-6 xl-4 col-grid-box"
                        : "col-xl-" + layoutColumns
                        }`}
                      key={i}
                    >
                      <ServiceItemV2 service={item} loading={loadingInitial} />
                    </div>
                  ))
                  }
                </div>
              </InfiniteScroll>)}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default observer(ServiceDashboardV2);
