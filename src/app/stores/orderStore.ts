import { action, computed, observable, runInAction } from "mobx";
import { toast } from "react-toastify";
import agent from "../api/agent";
import { IOrder, OrderStatus } from "../models/order";
import { RootStore } from "./rootStore";

export default class OrderStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @observable loadingOrders = false;
  @observable ordering = false;
  @observable order: IOrder | null = null;
  @observable orderRegistry = new Map();
  @observable submiting = false;
  @action loadOrder = (id: string) => {
    this.order = this.orderRegistry.get(id);
    return this.order;
  };
  @computed get ordersArray() {
    return Array.from(this.orderRegistry.values());
  }

  @action orderService = async (order: IOrder): Promise<IOrder> => {
    this.ordering = false;
    try {
      order.user = this.rootStore.userStore.user;
      var orderModel = await agent.Orders.order(order);
      runInAction(() => {
        this.ordering = false;
        toast.success("Order was succesfull");
      });
      return orderModel;
    } catch (error) {
      runInAction(() => {
        this.ordering = false;
      });
      toast.error("Order failed");
      console.log(error.problem);
    }
  };

  @action loadOrders = async (status?: OrderStatus, reload?: boolean): Promise<IOrder[]> => {
    if (!reload) this.loadingOrders = true;
    try {
      const orders = await agent.Orders.listForUser(status);
      runInAction(() => {
        orders.forEach((orders) => {
          this.orderRegistry.set(orders.id, orders);
        });
        if (!reload) this.loadingOrders = false;
      });
      return orders;
    } catch (error) {
      runInAction(() => {
        console.log(error);
        if (!reload) this.loadingOrders = false;
      });
    }
  };

  @action loadSentOrders = async (): Promise<IOrder[]> => {
    this.loadingOrders = true;
    try {
      const orders = await agent.Orders.listSentOrders();
      runInAction(() => {
        this.loadingOrders = false;
      });
      return orders;
    } catch (error) {
      runInAction(() => {
        console.log(error);
        this.loadingOrders = false;
      });
    }
  };

  @action changeStatus = async (
    orderId: string,
    status: OrderStatus
  ): Promise<IOrder> => {
    this.submiting = true;
    try {
      var response = await agent.Orders.changeStatus(orderId, status);
      runInAction(() => {
        this.order = response;
        this.orderRegistry.set(response.id, response);
        this.submiting = false;
      });
      toast.success(`Order status changed`);
      return response;
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      console.log(error);
      toast.error("Problem submitting data");
    }
  };
  @action loadOrderDetails = async (id: string): Promise<IOrder> => {
    let order = this.loadOrder(id);
    if (order) {
      this.order = order;
      this.loadingOrders = false;
      return order;
    } else {
      this.loadingOrders = true;
      try {
        order = await agent.Orders.details(id);
        runInAction(() => {
          this.order = order;
          this.orderRegistry.set(order.id, order);
          this.loadingOrders = false;
        });
        return this.order;
      } catch (error) {
        runInAction(() => {
          this.loadingOrders = false;
        });
      }
    }
  };
}
