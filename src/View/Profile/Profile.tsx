import { Col, Rate, Row } from "antd";
import { observer } from "mobx-react-lite";
import React, { Fragment, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";
import { DateDisplay } from "../../app/common/date-display/DateDisplay";
import { LoadingComponent } from "../../app/layout/LoadingComponent";
import NotFound from "../../app/layout/NotFound";
import { IService, ServiceStatus } from "../../app/models/service";
import { RootStoreContext } from "../../app/stores/rootStore";
import errorImg from "../../assets/images/search-not-found.png";

interface RouteParams {
  username: string;
}

interface IProps extends RouteParams { }

const Profile: React.FC<RouteComponentProps<IProps>> = ({ match }) => {
  const rootStore = useContext(RootStoreContext);
  const { profile, loadingProfile, loadProfile, userServicseByStatus } =
    rootStore.profileStore;
  const { loadUserServices } = rootStore.serviceStore;
  const [activeServices, setActiveServices] = useState<IService[]>(null);
  const { t } = useTranslation();

  useEffect(() => {
    loadProfile(match.params.username).then(() =>
      loadUserServices(match.params.username).then((services) => {
        setActiveServices(
          services.filter((x) => x.status === ServiceStatus.Active)
        );
      })
    );
  }, [loadProfile, match, userServicseByStatus, loadUserServices]);

  if (loadingProfile) return <LoadingComponent content={t("LoadingIndicators.LoadingProfile")} />;
  if (!!!profile) return <NotFound />;

  return (
    <Fragment>
      <div className="page-wrapper" style={{ paddingTop: "77px" }}>
        <div className="page-body-wrapper">
          <div className="page-body" style={{ marginTop: "10px" }}>
            <div className="container-fluid">
              <div className="user-profile">
                <div className="row">
                  {/* <!-- user profile first-style start--> */}
                  <div className="col-sm-12">
                    <div className="card hovercard text-center">
                      <div className="cardheader"></div>

                      <div className="user-image ">
                        <div className="avatar ">
                          <img
                            className="pro"
                            alt=""
                            src={profile.image || "/assets/user.png"}
                            data-intro="This is Profile image"
                          />
                        </div>
                        {/* <div className="icon-wrapper">
                          <i
                            className="icofont icofont-pencil-alt-5"
                            data-intro="Change Profile image here"
                          >
                            <input
                        className="pencil"
                        type="file"
                        onChange={(e) => readUrl(e)}
                      /> 
                          </i>
                        </div>*/}
                      </div>

                      <div className="info">
                        <div
                          className="row detail"
                          data-intro="This is the your details"
                        >
                          <div className="col-sm-6 col-lg-4 order-sm-1 order-xl-0">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="ttl-info text-left">
                                  <h6>
                                    <i className="fa fa-envelope mr-2"></i>
                                    {t("Profile.Email")}
                                  </h6>
                                  <span>{profile.email}</span>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="ttl-info text-left ttl-sm-mb-0">
                                  <h6>
                                    <i className="fa fa-calendar"></i>
                                    {t("Profile.Birthday")}
                                  </h6>
                                  <DateDisplay
                                    value={profile.birthday}
                                    format="DD/MM/YYYY"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-sm-12 col-lg-4 order-sm-0 order-xl-1">
                            <div className="user-designation">
                              <div className="title">
                                <a target="_blank" href="javascript">
                                  {profile.username}
                                </a>
                              </div>
                              <div className="desc mt-2">
                                {profile.firstName + " " + profile.lastName}
                              </div>
                            </div>
                          </div>
                          <div className="col-sm-6 col-lg-4 order-sm-2 order-xl-2">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="ttl-info text-left ttl-xs-mt">
                                  <h6>
                                    <i className="fa fa-phone"></i>
                                    {t("Profile.PhoneNumber")}
                                  </h6>
                                  <span>{profile.phoneNumber}</span>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="ttl-info text-left ttl-sm-mb-0">
                                  <h6>
                                    <i className="fa fa-location-arrow"></i>
                                    {t("Profile.Address")}
                                  </h6>
                                  <span>{profile.address}</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <br />
                        <div>
                          <div className="user-designation ttl-info">
                            <h6>{t("Profile.Bio")}</h6>
                            <span>{profile.bio || t("Profile.NoBio")}</span>
                          </div>
                        </div>
                        <hr />
                        <div
                          className="social-media"
                          data-intro="This is your Social details"
                        >
                          <ul className="list-inline">
                            {profile.faceBookLink ? (
                              <li className="list-inline-item">
                                <a href={profile.faceBookLink}>
                                  <i className="fa fa-facebook"></i>
                                </a>
                              </li>
                            ) : (
                              ""
                            )}
                            {profile.twitterLink ? (
                              <li className="list-inline-item">
                                <a href={profile.twitterLink}>
                                  <i className="fa fa-twitter"></i>
                                </a>
                              </li>
                            ) : (
                              ""
                            )}
                            {profile.instagramLink ? (
                              <li className="list-inline-item">
                                <a href={profile.instagramLink}>
                                  <i className="fa fa-instagram"></i>
                                </a>
                              </li>
                            ) : (
                              ""
                            )}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- user services--> */}

              <div className="col-xl-auto">
                <h5>{t("Profile.Services")}</h5>
                <br />
                {activeServices === null || activeServices?.length === 0 ? (
                  <div className="search-not-found text-center">
                    <div>
                      <img
                        className="img-fluid second-search"
                        src={errorImg}
                        alt=""
                      />
                      <p>{t("Profile.NoServices")}</p>
                    </div>
                  </div>
                ) : (
                  activeServices?.map((data, i) => (
                    <div className="card" key={i}>
                      <div className="job-search">
                        <div className="card-body">
                          <div className="media">
                            <img
                              className="img-40 img-fluid m-r-20"
                              src={data.image || "/assets/imageplaceholder.png"}
                              alt=""
                            />
                            <div className="media-body">
                              <h6 className="f-w-600">
                                <Link to={`/service/${data.id}`}>
                                  {data.name}
                                </Link>
                              </h6>
                              <div>
                                <Row gutter={16}>
                                  <Col>
                                    <p>
                                      {data.price} {t("Profile.HrkPerH")}
                                    </p>
                                  </Col>

                                  <Rate
                                    defaultValue={data.rating}
                                    allowHalf={true}
                                    disabled={true}
                                    style={{ color: "#0000ff", fontSize: 14 }}
                                  />
                                </Row>
                              </div>
                            </div>
                          </div>
                          <p>{data.introduction}</p>
                        </div>
                      </div>
                    </div>
                  ))
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default observer(Profile);
