import { Button, Col, Form, Rate, Row } from "antd";
import { useForm } from "antd/lib/form/Form";
import TextArea from "antd/lib/input/TextArea";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import { INotification } from "../../../models/notification";
import { RootStoreContext } from "../../../stores/rootStore";

interface Props {
  notification: INotification;
}

const NotificationReviewModal: React.FC<Props> = ({ notification }) => {
  const rootStore = useContext(RootStoreContext);
  const { toggleIsRead } = rootStore.notificationStore;
  const { closeModal } = rootStore.modalStore;
  const { t } = useTranslation();
  const [ratingform] = useForm();
  const { createReview } = rootStore.serviceStore;

  const handleSave = () => {
    ratingform.validateFields().then((fields) => {
      createReview(fields, notification.orderedService.id)
        .then(() => toggleIsRead(notification))
        .finally(() => closeModal());
    });
  };

  return (
    <div>
      <p style={{ textAlign: "center" }}>
        {t("ReviewForm.Title", { service: notification.orderedService.name })}
      </p>
      <Form form={ratingform} layout="vertical">
        <Row gutter={16}>
          <Col flex="auto">
            <Form.Item label={t("ReviewForm.Comment")} name="text" required>
              <TextArea rows={2} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col flex="auto">
            <Form.Item label={t("ReviewForm.Rating")} name="rate" required>
              <Rate />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={4} offset={8}>
            <Button danger block onClick={() => closeModal()}>
              {t("ReviewForm.Close")}
            </Button>
          </Col>
          <Col span={4}>
            <Button type="primary" block onClick={() => handleSave()}>
              {t("ReviewForm.Submit")}
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
export default observer(NotificationReviewModal);
