import { FieldRenderProps } from "react-final-form";
import React from "react";
import { FormFieldProps } from "semantic-ui-react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
/** @jsx jsx */
import { css, jsx } from "@emotion/core";

interface IProps extends FieldRenderProps<Date, HTMLElement>, FormFieldProps {
  id?: string;
}

export const DateInputV2: React.FC<IProps> = ({
  input,
  placeholder,
  label,
  dateFormat,
  showMonthDropdown,
  showYearDropdown,
  dropdownMode,
  peekNextMonth,
  minDate,
  maxDate,
  disabled,
  meta: { touched, error },
  ...rest
}) => {
  return (
    <div className="form-group">
      {label && <label className="col-form-label">{label}</label>}
      <div className="datepicker-here" data-language="hr">
        <DatePicker
          className="form-control digits"
          onChange={input.onChange}
          selected={input.value}
          placeholderText={placeholder}
          dateFormat={dateFormat}
          showMonthDropdown={showMonthDropdown}
          showYearDropdown={showYearDropdown}
          dropdownMode={dropdownMode}
          minDate={minDate}
          maxDate={maxDate}
          disabled={disabled}
          {...rest}
        />
      </div>
      {touched && error && (
        <div
          key={error}
          css={css`
            font-size: 12px;
            color: red;
          `}
        >
          {error}
        </div>
      )}
    </div>
  );
};
