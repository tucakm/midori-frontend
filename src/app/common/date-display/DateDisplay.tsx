import moment, { Moment } from "moment";
import React from "react";

interface DateDisplayProps {
  format: string;
  value: Moment;
}

export const DateDisplay: React.FC<DateDisplayProps> = ({ format, value }) => {
  return <span>{moment(value).format(format)}</span>;
};
