import { Divider, Layout, Menu } from "antd";
import { Content, Footer } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import React, { Fragment } from "react";
import { Edit } from "react-feather";
import { Link } from "react-router-dom";

export const Sidebar: React.FC = () => {
  return (
    <Layout>
      <Sider
        collapsible
        reverseArrow
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
        theme="light"
      >
        <Fragment>
          <div className="sidebar-user text-center logo p-25">
            <div>
              <Link to={`user-dashboard/edit`}>
                <img
                  className="img-60 rounded-circle align-self-center avatar-showcase"
                  // src={currentUser ? currentUser.image : ""}
                  alt="#"
                />
                <div className="profile-edit">
                  <Edit />
                </div>
              </Link>
            </div>
            {/* <h6 className="mt-3 f-14">{currentUser?.username}</h6>*/}
          </div>
        </Fragment>
        <Divider type="horizontal" />
        <Menu theme="light" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
          <Menu.Item key="4">nav 4</Menu.Item>
          <Menu.Item key="5">nav 5</Menu.Item>
          <Menu.Item key="6">nav 6</Menu.Item>
          <Menu.Item key="7">nav 7</Menu.Item>
          <Menu.Item key="8">nav 8</Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Content>
          <div style={{ padding: 24, textAlign: "center" }}>
            <p>test</p>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}></Footer>
      </Layout>
    </Layout>
  );
};
