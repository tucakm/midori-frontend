import axios, { AxiosResponse } from "axios";
import { history } from "../..";
import { toast } from "react-toastify";
import { IUser, IUserFormValues, IUserProfile } from "../models/user";
import { IService, IServicesWrapper, ServiceStatus } from "../models/service";
import { ITag } from "../models/tag";
import { IProfile, IPhoto } from "../models/profile";
import { IOrder, OrderStatus } from "../models/order";
import { INotification } from "../models/notification";
import { IReview } from "../models/review";
axios.defaults.baseURL = "http://localhost:5000/api";
axios.interceptors.request.use(
  (config) => {
    const token = window.localStorage.getItem("jwt");
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(undefined, (error) => {
  if (error.message === "Network Error" && !error.response) {
    toast.error("Network error -API is not running");
  }
  const { status, data, config } = error.response;
  if (error.response.status === 404) {
    history.push("/notfound");
  }
  if (
    status === 400 &&
    config.method === "get" &&
    data.errors.hasOwnProperty("id")
  ) {
    history.push("/notfound");
  }
  if (status === 500) {
    toast.error("Server error");
  }
  throw error.response;
});

const responseBody = (response: AxiosResponse) => response.data;

const sleep = (ms: number) => (response: AxiosResponse) =>
  new Promise<AxiosResponse>((resolve) =>
    setTimeout(() => resolve(response), ms)
  );

const requests = {
  get: (url: string) => axios.get(url).then(sleep(0)).then(responseBody),
  post: (url: string, body: {}) =>
    axios.post(url, body).then(sleep(0)).then(responseBody),
  put: (url: string, body: {}) =>
    axios.put(url, body).then(sleep(0)).then(responseBody),
  del: (url: string) => axios.delete(url).then(sleep(0)).then(responseBody),
  patch: (url: string, body: {}) => {
    return axios
      .patch(url, body, {
        headers: { "Content-type": "application/json" },
      })
      .then(responseBody);
  },
  postForm: (url: string, file: Blob) => {
    let formData = new FormData();
    formData.append("File", file);
    return axios
      .post(url, formData, {
        headers: { "Content-type": "multipart/form-data" },
      })
      .then(responseBody);
  },
};

const User = {
  curent: (): Promise<IUser> => requests.get("/user"),
  updateProfile: (profile: IUserProfile) => requests.put("user/edit", profile),
  login: (user: IUserFormValues): Promise<IUser> =>
    requests.post(`/user/login`, user),
  register: (user: IUserFormValues): Promise<IUser> =>
    requests.post(`user/register`, user),
  fbLogin: (accessToken: string) =>
    requests.post(`/user/facebook/`, { accessToken }),
  checkOrders: (serviceId: string) =>
    requests.get(`/user/service/${serviceId}/check-order`)
};
const Profiles = {
  get: (username: string): Promise<IProfile> =>
    requests.get(`/profiles/${username}`),
  addProfilePhoto: (photo: Blob): Promise<IPhoto> =>
    requests.postForm(`/photos`, photo),
  updateProfile: (profile: Partial<IProfile>) =>
    requests.put("/profiles", profile),
};

const Services = {
  list: (
    limit?: number,
    page?: number,
    criteria?: string,
    status?: ServiceStatus,
    sort?: string
  ): Promise<IServicesWrapper> =>
    requests.get(
      `/service?limit=${limit}&offset=${page ? page * limit! : 0}&criteria=${criteria}${sort ? `&sort=${sort}` : ""}&status=${status ? status : ServiceStatus.Active
      }`
    ),
  details: (id: string) => requests.get(`/service/${id}`),
  create: (service: IService): Promise<IService> =>
    requests.put("/service", service),
  delete: (id: string) => requests.del(`/service/${id}`),
  listUserServices: (username: string): Promise<IService[]> =>
    requests.get(`/service/profile/${username}`),
  listTags: (): Promise<ITag[]> => requests.get("/service/tags"),
  uploadPhotoForService: (photo: Blob, id: string): Promise<IPhoto> =>
    requests.postForm(`/photos/service/${id}`, photo),
  createTagForService: (serviceId: string, tags: string[]) =>
    requests.post("/service/tags/create", { serviceId, tags }),
  changeStatus: (serviceId: string, status: number): Promise<IService> =>
    requests.patch(`/service/${serviceId}/change-status`, status),

  addReview: (review: IReview, serviceId: string): Promise<IReview> =>
    requests.put(`/service/${serviceId}/send-review`, review),
};
const Orders = {
  details: (id: string) => requests.get(`/order/${id}`),
  listForUser: (status?: OrderStatus): Promise<IOrder[]> => {
    if (status) return requests.get(`/order?orderStatus=${status}`);
    else return requests.get("/order");
  },
  order: (order: IOrder): Promise<IOrder> => requests.put("/order", order),
  changeStatus: (orderId: string, status: OrderStatus): Promise<IOrder> =>
    requests.patch(`/order/${orderId}/change-status`, status),
  listSentOrders: (): Promise<IOrder[]> => requests.get("order/sent"),
};
const Notifications = {
  listForUser: (username: string): Promise<INotification[]> =>
    requests.get(`/notification/${username}`),
  toggleIsRead: (id: string, isRead: boolean): Promise<INotification> =>
    requests.patch(`/notification/${id}/toggleIsRead`, isRead),
};

export default {
  User,
  Services,
  Profiles,
  Orders,
  Notifications,
};
