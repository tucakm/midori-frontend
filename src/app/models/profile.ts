import { Moment } from "moment";
import { IService } from "./service";

export interface IProfile {
  username?: string;
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  image?: string;
  photo?: IPhoto;
  service?: IService[];
  birthday?: Moment;

  //profile form
  bio?: string;
  phoneNumber?: string;
  job?: string;
  website?: string;
  city?: string;
  company?: string;
  zipCode?: string;
  country?: string;
  address?: string;
  faceBookLink?: string;
  twitterLink?: string;
  instagramLink?: string;
}
export interface IPhoto {
  id: string;
  url: string;
}
