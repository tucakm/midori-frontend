import { SearchOutlined } from "@ant-design/icons";
import { Badge, Button, Col, Input, Row, Spin } from "antd";
import Modal from "antd/lib/modal/Modal";
import { debounce } from "lodash";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { IService, ServiceStatus } from "../../../app/models/service";
import { RootStoreContext } from "../../../app/stores/rootStore";
import ServiceItemV2 from "../../ServiceProfile/ServiceItemV2";
import errorImg from "../../../assets/images/search-not-found.png";


const Main = () => {
  const rootStore = useContext(RootStoreContext);
  const { profile } = rootStore.profileStore;
  const { deleteService, loadUserServices, loadingServices } =
    rootStore.serviceStore;
  const history = useHistory();
  const { t } = useTranslation();
  const [deteleModel, setDeleteModel] = useState<IService>(null);
  const [services, setServices] = useState<IService[]>(null);
  const [servicesFiltered, setServicesFiltered] = useState<IService[]>(null);
  const [open, show] = useState(false);

  useEffect(() => {
    loadUserServices(profile.username).then((response) => {
      setServices(response);
      setServicesFiltered(response);
    });
  }, [profile.username, loadUserServices]);

  const handleClose = () => {
    setDeleteModel(null);
    show(false);
  };
  const handleDelete = () => {
    deleteService(deteleModel);
    show(false);
    setServices((services) => services.filter((x) => x.id !== deteleModel.id));
    setServicesFiltered((services) =>
      services.filter((x) => x.id !== deteleModel.id)
    );
  };
  const handleBadgeColor = (status: ServiceStatus) => {
    if (status === ServiceStatus.Active) return "green";
    else if (status === ServiceStatus.Draft) return "orange";
    else if (status === ServiceStatus.Inactive) return "red";
    else return "grey";
  };
  const handleOnDelete = (service: IService) => {
    setDeleteModel(service);
    show(true);
  };
  const onSearch = debounce((value: string) => {
    setServicesFiltered(
      services.filter(
        (x) => x.name.toLowerCase().indexOf(value.toLowerCase()) > -1
      )
    );
  }, 500);
  if (!services)
    return <div style={{ left: "50 %", top: "50 %", position: "fixed" }}>
      <Spin />
    </div>
  return (
    <div className="container-flud card">
      <div className="p-20">
        <Row gutter={16}>
          <Col span={24} className="m-b-20">
            <Row gutter={16}>
              <Col span={8}>
                <Input
                  prefix={<SearchOutlined />}
                  onChange={(e) => onSearch(e.target.value)}
                />
              </Col>
              <Col span={4} offset={12}>
                <Button
                  type="primary"
                  block
                  onClick={() => history.push("/createservice")}
                >
                  {t("UserDashboard.Services.AddButton")}
                </Button>
              </Col>
            </Row>
          </Col>
          {servicesFiltered ? (
            servicesFiltered.map((service: IService, _i) => (
              <Col span={6} key={_i}>
                <Badge
                  color={handleBadgeColor(service.status)}
                  size="default"
                  style={{ width: 15, height: 15 }}
                >
                  <ServiceItemV2
                    service={service}
                    editing={true}
                    onDelete={() => handleOnDelete(service)}
                    onEdit={() => history.push(`/manage/${service.id}`)}
                  />
                </Badge>
              </Col>
            ))
          ) : (
            <Col span={24}>
              <div className="search-not-found text-center">
                <img
                  className="img-fluid second-search"
                  src={errorImg}
                  alt=""
                />
                <p>{t("UserDashboard.Services.NoServices")}</p>
              </div>
            </Col>
          )}
        </Row>

        <Modal
          visible={open}
          title={t("UserDashboard.Services.ModalDeleteTitle")}
          onOk={() => handleDelete()}
          onCancel={() => handleClose()}
          confirmLoading={loadingServices}
        >
          <p>{t("UserDashboard.Services.ModalDeleteContent")}</p>
        </Modal>
      </div>
    </div>
  );
};

export default observer(Main);
