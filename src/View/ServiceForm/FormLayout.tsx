import { Button, Col, Layout, Row, Steps } from "antd";
import { Content, Footer } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router-dom";
import { LoadingComponent } from "../../app/layout/LoadingComponent";
import NotFound from "../../app/layout/NotFound";
import { ServiceFormValues, ServiceStatus } from "../../app/models/service";
import { RootStoreContext } from "../../app/stores/rootStore";
import { useServiceFormContext } from "../../app/util/hooks";
import ServiceFormV2 from "./Form/ServiceFormV2";
import ImageForm from "./ImageForm/ImageForm";
import Summary from "./Sumary/Summary";

const { Step } = Steps;

const FormLayout: React.FC = () => {
  const serviceContext = useServiceFormContext();
  const {
    currentStep,
    serviceModel,
    handleNextButtonClick,
    setServiceModel,
    handlePreviousClick,
    stepSwitching,
  } = serviceContext;

  const rootStore = useContext(RootStoreContext);
  const params = useParams<{ id: string }>();
  const {
    loadService,
    submiting,
    uploadingPhoto,
    changeStatus,
  } = rootStore.serviceStore;
  const { user } = rootStore.userStore;
  const history = useHistory();
  const { t } = useTranslation();
  useEffect(() => {
    if (params.id)
      loadService(params.id).then((service) =>
        setServiceModel(new ServiceFormValues(service))
      );
    else setServiceModel(new ServiceFormValues());
  }, [params.id, loadService, setServiceModel]);

  const steps = [
    {
      title: t("ServiceWizard.GeneralInfo"),
      content: (
        <ServiceFormV2 onStepSwitch={(isSwitch) => stepSwitching(isSwitch)} />
      ),
      order: 0,
      requred: true,
      description: t("ServiceWizard.GeneralInfoRequired"),
    },
    {
      title: t("ServiceWizard.Image"),
      content: (
        <ImageForm onStepSwitch={(isSwitch) => stepSwitching(isSwitch)} />
      ),
      order: 1,
      requred: false,
    },
    {
      title: t("ServiceWizard.Summary"),
      content: <Summary />,
      order: 2,
      requred: false,
    },
  ];
  const handleChangeStatus = (status?: ServiceStatus) => {
    changeStatus(serviceModel.id, status).then((resposne) => {
      if (resposne) historyPush(resposne.id);
    });
  };
  const historyPush = (serviceId: string) => {
    history.push(`/service/${serviceId}`);
  };

  const handleSaveForLater = () => {
    history.push("/user-dashboard/my/services");
  };
  if (!serviceModel) return <LoadingComponent content={t("LoadingIndicators.Loading")} />;
  if (params.id && serviceModel.user?.username !== user.username)
    return <NotFound />;
  return (
    <Layout style={{ background: "#ffff", paddingTop: "77px" }}>

      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
        theme="light"
      >
        <Steps
          current={serviceContext.currentStep}
          direction="vertical"
          style={{ margin: 24, paddingTop: 50 }}
        >
          {steps.map((item) => (
            <Step
              key={item.title}
              title={item!.title}
              description={item.description}
              style={{ paddingTop: 5 }}
            //onClick={() => handeWhenStepSwitching(item.order, false, false)}
            />
          ))}
        </Steps>
      </Sider>
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64, marginLeft: 110, background: "#ffff" }}
      >
        {steps[serviceContext.currentStep].content}
      </Content>
      <Footer
        style={{
          position: "fixed",
          left: 0,
          bottom: 0,
          width: "100%",
          background: "#ffff",
        }}
      >
        <Row gutter={16}>
          <Col span={4}>
            <Button block onClick={() => history.goBack()}>
              {t("ServiceWizard.Cancel")}
            </Button>
          </Col>
          <Col span={4} offset={4}>
            <Button
              block
              disabled={currentStep === 0}
              onClick={handlePreviousClick}
            >
              {t("ServiceWizard.Previous")}
            </Button>
          </Col>
          <Col span={4}>
            <Button
              block
              loading={submiting || uploadingPhoto}
              onClick={() => handleNextButtonClick()}
              disabled={!(steps.length - 1 > currentStep)}
            >
              {t("ServiceWizard.Next")}
            </Button>
          </Col>
          {steps.length - 1 === currentStep && (
            <>
              {serviceModel.status !== ServiceStatus.Active ? (
                <Col span={4}>
                  <Button
                    block
                    onClick={() => handleSaveForLater()}
                    disabled={steps.length - 1 !== currentStep}
                  >
                    {t("ServiceWizard.SaveForLater")}
                  </Button>
                </Col>
              ) : (
                <Col span={4}>
                  <Button
                    block
                    danger
                    onClick={() => handleChangeStatus(ServiceStatus.Inactive)}
                    disabled={steps.length - 1 !== currentStep}
                  >
                    {t("ServiceWizard.SetInactive")}
                  </Button>
                </Col>
              )}
              <Col span={4}>
                <Button
                  type="primary"
                  block
                  onClick={() => {
                    serviceModel.status !== ServiceStatus.Active
                      ? handleChangeStatus(ServiceStatus.Active)
                      : historyPush(serviceModel.id);
                  }}
                  disabled={steps.length - 1 !== currentStep}
                >
                  {serviceModel.status !== ServiceStatus.Active
                    ? t("ServiceWizard.Publish")
                    : t("ServiceWizard.Save")}
                </Button>
              </Col>
            </>
          )}
        </Row>
      </Footer>
    </Layout>
  );
};
export default observer(FormLayout);
