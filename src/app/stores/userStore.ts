import { observable, computed, action, runInAction } from "mobx";
import { IUser, IUserFormValues, IUserProfile } from "../models/user";
import agent from "../api/agent";
import { RootStore } from "./rootStore";
import { history } from "../..";
import { toast } from "react-toastify";

export default class UserStore {
  rootStore: RootStore;
  constructor(Rootstore: RootStore) {
    this.rootStore = Rootstore;
  }

  @observable user: IUser | null = null;
  @observable loading = false;
  @observable userProfile: IUserProfile | null = null;
  @observable submiting = false;
  @observable ordering = false;

  @computed get isLoggedIn() {
    return !!this.user;
  }

  @action login = async (values: IUserFormValues) => {
    try {
      const user = await agent.User.login(values);
      runInAction(() => {
        this.user = user;
      });
      this.rootStore.commonStore.setToken(user.token);

      this.rootStore.modalStore.closeModal();
      history.push("/");
    } catch (error) {
      throw error;
    }
  };

  @action register = async (values: IUserFormValues) => {
    try {
      const user = await agent.User.register(values);
      this.rootStore.commonStore.setToken(user.token);
      this.rootStore.modalStore.closeModal();
      history.push("/");
    } catch (error) {
      throw error;
    }
  };
  @action logout = () => {
    this.rootStore.commonStore.setToken(null);
    this.user = null;
    history.push("/");
  };
  @action fbLogin = async (response: any) => {
    this.loading = true;
    try {
      const user = await agent.User.fbLogin(response.accessToken);
      runInAction(() => {
        this.user = user;
        this.rootStore.commonStore.setToken(user.token);
        this.rootStore.modalStore.closeModal();
        this.loading = false;
      });
      history.push("/");
    } catch (error) {
      this.loading = false;
      throw error;
    }
  };

  @action getUser = async () => {
    try {
      const user = await agent.User.curent();
      runInAction(() => {
        this.user = user;
      });
    } catch (error) {
      console.log(error);
    }
  };
  @action checkOrder = async (serviceId): Promise<boolean> => {
    try {
      const hasOrder = await agent.User.checkOrders(serviceId);

      return hasOrder;
    } catch (error) {
      this.loading = false;
      throw error;
    }
  }

  @action editUserProfile = async (profile: IUserProfile) => {
    this.submiting = false;
    try {
      await agent.Profiles.updateProfile(profile);
      runInAction(() => {
        this.userProfile = profile;
        this.submiting = false;
        toast.success("Updated profile");
      });
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      toast.error("Problem updating profile");
      console.log(error.problem);
    }
  };
}
