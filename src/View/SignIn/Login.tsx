import React, { Fragment, useContext } from "react";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "../../app/stores/rootStore";
import { Form as FinalForm, Field } from "react-final-form";
//import { Form, Values, required, minLength } from "../../app/common/form/Form";
//import { Field } from "../../app/common/form/Field";
import { IUserFormValues } from "../../app/models/user";
import { FORM_ERROR } from "final-form";
import { combineValidators, isRequired } from "revalidate";
import { ErrorMessage } from "../../app/common/form/ErrorMessage";
import { TextInput } from "../../app/common/form/TextInput";
import { Button } from "semantic-ui-react";
import SocialLogins from "./SocialLogins/SocialLogins";

//import { Form, Button } from "semantic-ui-react";

const validate = combineValidators({
  email: isRequired("Email"),
  password: isRequired("Password"),
});

const Login = () => {
  const rootStore = useContext(RootStoreContext);
  const { login } = rootStore.userStore;

  return (
    <Fragment>
      <div className="page-wrapper">
        <div className="auth-bg">
          <div className="authentication-box">
            <div className="card mt-4">
              <div className="card-body">
                <div className="text-center">
                  <h4>Sign in</h4>
                  <h6>Enter your Username and Password </h6>
                </div>
                <FinalForm
                  onSubmit={(values: IUserFormValues) =>
                    login(values).catch((error) => ({
                      [FORM_ERROR]: error,
                    }))
                  }
                  validate={validate}
                  render={({
                    handleSubmit,
                    submitting,
                    submitError,
                    invalid,
                    pristine,
                    dirtySinceLastSubmit,
                  }) => (
                    <form
                      onSubmit={handleSubmit}
                      className="theme-form needs-validation"
                    >
                      <Field
                        placeholder="Email"
                        name="email"
                        component={TextInput}
                        label="Your Email"
                      />

                      <Field
                        placeholder="******"
                        name="password"
                        component={TextInput}
                        type="password"
                        label="Password"
                      />
                      {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage
                          error={submitError}
                          text="Invalid email or password"
                        />
                      )}
                      <div className="form-group form-row mt-3 mb-0">
                        <Button
                          className="btn btn-primary btn-block"
                          color="teal"
                          loading={submitting}
                          disabled={
                            invalid && (!dirtySinceLastSubmit || pristine)
                          }
                          fluid
                        >
                          Login
                        </Button>
                      </div>
                      <div className="login-divider"></div>
                      <SocialLogins />
                    </form>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default observer(Login);
