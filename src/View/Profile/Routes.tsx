import React from "react";
import { Route, RouteComponentProps, Switch } from "react-router-dom";
import NotFound from "../../app/layout/NotFound";
import Profile from "./Profile";

export const Routes: React.FC<RouteComponentProps> = ({ match }) => {
  return (
    <Switch>
      <Route path={match.path} exact component={Profile} />
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};
