import React, { Fragment } from "react";
import { Trans, useTranslation } from "react-i18next";

interface LanguageProps {
  onLanguageChange: (language: string) => void;
}

const Language: React.FC<LanguageProps> = ({ onLanguageChange }) => {
  const { i18n } = useTranslation();

  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language);
    onLanguageChange(language.toUpperCase());
  };
  return (
    <Fragment>
      <div>
        <ul className="language-dropdown onhover-show-div p-20">
          <li onClick={() => changeLanguage("en")}>
            <i className="flag-icon flag-icon-gb"></i>{" "}
            <Trans i18nKey="Language1" />
          </li>
          <li onClick={() => changeLanguage("hr")}>
            <i className="flag-icon flag-icon-hr"></i>{" "}
            <Trans i18nKey="Language2" />
          </li>
        </ul>
      </div>
    </Fragment>
  );
};

export default Language;
