import React from "react";
import { FieldRenderProps } from "react-final-form";
import { FormFieldProps } from "semantic-ui-react";
/** @jsx jsx */
import { css, jsx } from "@emotion/core";

interface IProps extends FieldRenderProps<string, HTMLElement>, FormFieldProps {
  readonly: boolean;
}

export const TextInput: React.FC<IProps> = ({
  input,
  type,
  placeholder,
  readonly,
  label,
  meta: { touched, error },
}) => {
  return (
    <div className="form-group">
      {label && <label className="col-form-label">{label}</label>}
      <input
        type={type}
        {...input}
        placeholder={placeholder}
        readOnly={readonly}
        className="form-control"
      />
      {touched && error && (
        <div
          key={error}
          css={css`
            font-size: 12px;
            color: red;
          `}
        >
          {error}
        </div>
      )}
    </div>
  );
};
