import { RootStore } from "./rootStore";
import { observable, action, runInAction, computed } from "mobx";
import { IProfile } from "../models/profile";
import agent from "../api/agent";
import { toast } from "react-toastify";
import { IService, ServiceStatus } from "../models/service";
import { computedFn } from "mobx-utils/lib/computedFn";
import { toJS } from 'mobx'
export default class ProfileStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @observable currentUserProfile: IProfile | null = null;
  @observable profile: IProfile | null = null;
  @observable loadingProfile = false;
  @observable uploadingPhoto = false;
  @observable reLoadingProfile = false;
  @observable submiting = false;

  @computed get isCurrentUser() {
    if (this.rootStore.userStore.user && this.profile) {
      return this.rootStore.userStore.user.username === this.profile.username;
    } else {
      return false;
    }
  }


  userServicseByStatus = computedFn((status: ServiceStatus) => {
    if (this.profile && this.profile.service && this.profile.service.length > 0) {
      const values = this.filterServicesRegistry(
        Array.from(toJS(this.profile.service)),
        status
      );
      console.log(values)
      return values
    }
  });

  filterServicesRegistry = (services: IService[], status: ServiceStatus) => {
    console.log(services);
    return services.filter(x => x.status === status);
  };

  @action loadProfile = async (username: string, reloading?: boolean) => {
    if (reloading != null || reloading) this.reLoadingProfile = true;
    else this.loadingProfile = true;
    try {
      const profile = await agent.Profiles.get(username);

      runInAction(() => {
        this.profile = profile;
        if (reloading != null || reloading) this.reLoadingProfile = false;
        else this.loadingProfile = false;
      });
    } catch (error) {
      runInAction(() => {
        if (reloading != null || reloading) this.reLoadingProfile = false;
        else this.loadingProfile = false;
      });
      console.log(error);
    }
  };

  @action updateProfile = async (profile: Partial<IProfile>) => {
    this.submiting = true;
    try {
      await agent.Profiles.updateProfile(profile);
      runInAction(() => {
        if (profile.username !== this.rootStore.userStore.user!.username) {
          this.rootStore.userStore.user!.username = profile.username!;
        }
        this.profile = { ...this.profile!, ...profile };
        this.submiting = false;
        toast.success("Updated profile");
      });
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      toast.error("Problem updating profile");
      console.log(error.problem);
    }
  };

  @action uploadPhoto = async (file: Blob) => {
    this.uploadingPhoto = true;
    try {
      const photo = await agent.Profiles.addProfilePhoto(file);
      console.log(this.uploadingPhoto);
      runInAction(() => {
        if (this.profile) {
          this.profile.photo = photo;
        }
        this.uploadingPhoto = false;
      });
    } catch (error) {
      console.log(error);
      toast.error("Problem uploading photo");
      runInAction(() => {
        this.uploadingPhoto = false;
      });
    }
  };
}
