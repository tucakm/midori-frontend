import React, { Fragment, useContext } from "react";
import { User, Settings, LogOut } from "react-feather";
import { Link } from "react-router-dom";
import { RootStoreContext } from "../../stores/rootStore";
import { IUser } from "../../models/user";
import { observer } from "mobx-react-lite";
import { useTranslation } from "react-i18next";

interface Props {
  user: IUser;
}

const UserMenu: React.FC<Props> = ({ user }) => {
  const rootStore = useContext(RootStoreContext);
  const { logout } = rootStore.userStore;
  const { t } = useTranslation();
  return (
    <Fragment>
      <li className="onhover-dropdown">
        <div className="media align-items-center">
          <img
            className="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded"
            src={user!.image || "/assets/user.png"}
            alt="header-user"
          />
          <div className="dotted-animation">
            <span className="animate-circle"></span>
            <span className="main-circle"></span>
          </div>
        </div>
        <ul className="profile-dropdown onhover-show-div p-20 profile-dropdown-hover">
          <li>
            <Link to={`/profile/${user.username}`}>
              <User />
              {t("Header.UserMenu.Profile")}
            </Link>
          </li>
          <li>
            <Link to={`/user-dashboard/my/services`}>
              <Settings />
              {t("Header.UserMenu.Dashboard")}
            </Link>
          </li>
          <li>
            <a onClick={logout} href="#!">
              <LogOut /> {t("Header.UserMenu.LogOut")}
            </a>
          </li>
        </ul>
      </li>
    </Fragment>
  );
};

export default observer(UserMenu);
