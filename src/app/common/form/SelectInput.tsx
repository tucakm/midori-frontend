import React from "react";
import { FieldRenderProps } from "react-final-form";
import { FormFieldProps, Form, Label, Select } from "semantic-ui-react";
/** @jsx jsx */
import { css, jsx } from "@emotion/core";

interface IProps
  extends FieldRenderProps<string, HTMLElement>,
    FormFieldProps {}
export const SelectInput: React.FC<IProps> = ({
  input,
  width,
  options,
  placeholder,
  meta: { touched, error },
}) => {
  return (
    <Form.Field error={touched && !!error} width={width}>
      <Select
        className="form-control btn-square"
        value={input.value}
        onChange={(e, data) => input.onChange(data.value)}
        placeholder={placeholder}
        options={options}       
      ></Select>

      {touched && error && (
        <div
          key={error}
          css={css`
            font-size: 12px;
            color: red;
          `}
        >
          {error}
        </div>
      )}
    </Form.Field>
  );
};
