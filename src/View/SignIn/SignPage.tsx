import React, { useContext } from "react";
import { RootStoreContext } from "../../app/stores/rootStore";
import { Segment, Container, Grid, Header } from "semantic-ui-react";
import { LoginForm } from "../user/LoginForm";

export const SignPage = () => {
  const rootStore = useContext(RootStoreContext);
  return (
    <Segment inverted textAlign="center" vertical className="masthead">
      <Container>
        <Grid columns={2} relaxed="very" stackable>
          <Grid.Column verticalAlign="middle">
            <Header as="h1" inverted>
              JobQuest
            </Header>
          </Grid.Column>
          <Grid.Column>
            <LoginForm/>

          </Grid.Column>
        </Grid>
      </Container>
    </Segment>
  );
};
