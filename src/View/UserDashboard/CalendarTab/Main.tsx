import { Col, List, Modal, Row, Spin } from "antd";
import { observer } from "mobx-react-lite";
import React, {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import FullCalendar, { EventDropArg } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin, { Draggable } from "@fullcalendar/interaction";
import { RootStoreContext } from "../../../app/stores/rootStore";
import { IOrder, OrderStatus } from "../../../app/models/order";
import OrderInfo from "./OrderInfo";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { Banner } from "./Banner";
import allLocales from '@fullcalendar/core/locales-all';

function getStratEditable(status: OrderStatus): boolean {
  if (status === OrderStatus.Pending || status === OrderStatus.RescheduleNeeded)
    return true;
  else return false;
}
const Main: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { loadOrders, orderService, loadOrderDetails, loadingOrders } = rootStore.orderStore;
  const { openModal } = rootStore.modalStore;
  const [eventList, setEvenetList] = useState<any>([]);

  const { t, i18n } = useTranslation();

  const getBackgroundColor = (status: OrderStatus) => {
    if (status === OrderStatus.Pending) return "orange";
    else if (status === OrderStatus.RescheduleNeeded) return "red";
    else if (status === OrderStatus.Finished) return "grey";
    else if (status === OrderStatus.Aproved) return "green";
    else if (status === OrderStatus.RescheduleSent) return "purple";
  };
  const [todaysOrders, setTodaysOrders] = useState<IOrder[]>();

  const calculateCallendarEvents = useCallback((orders: IOrder[]) => {
    var listEvents = [];
    if (orders) {
      orders.forEach((order: IOrder) => {
        listEvents.push({
          id: order.id,
          title: `${order.service.name}/${order.id}`,
          start: order.startTime,
          end: order.endTime,
          backgroundColor: getBackgroundColor(order.status),
          startEditable: getStratEditable(order.status),
        });
      });
    }
    return listEvents;
  }, []);

  const ref = useRef();
  useEffect(() => {
    loadOrders().then((orders) => {
      setEvenetList(calculateCallendarEvents(orders));
      setTodaysOrders(orders.filter((x) => x.status === OrderStatus.Aproved));
    });
  }, [loadOrders, calculateCallendarEvents]);

  const reloadOrers = () => {
    loadOrders(null, true).then((orders) => {
      setEvenetList(calculateCallendarEvents(orders));
      setTodaysOrders(orders.filter((x) => x.status === OrderStatus.Aproved));
    });
  };
  useEffect(() => {
    let draggableEl = document.getElementById("external-events");
    if (draggableEl) new Draggable(draggableEl, {
      itemSelector: ".fc-event",
      eventData: function (eventEl) {
        let title = eventEl.getAttribute("title");
        let id = eventEl.getAttribute("data");
        return {
          title: title,
          id: id,
        };
      },
    });
  }, []);
  const saveReschedule = async (event: EventDropArg) => {
    const { start, end, id } = event.event;
    var order: IOrder;
    order = await loadOrderDetails(id);
    orderService({
      ...order,
      date: moment(start).utc(true).startOf("day"),
      startTime: moment(start).utc(true),
      endTime: moment(end).utc(true),
      status: OrderStatus.RescheduleSent,
    }).then((response) => {
      if (response) reloadOrers();
    });
  };
  const handleReschedule = (event: EventDropArg) => {
    Modal.confirm({
      title: t("Calendar.ModalConfirmReschedule"),
      onOk: () => saveReschedule(event),
      onCancel: () => event.revert(),
    });
  };
  if (loadingOrders)
    return (
      <div style={{ width: "50 %" }}>
        <Spin />
      </div>
    );

  return (
    <Fragment>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            <div className="card">
              <div className="card-header">
                <h5>{t("Calendar.Title")}</h5>
              </div>
              <div className="card-body">
                <div className="animated fadeIn demo-app">
                  <Row>
                    <Col xl={6} md={6}>
                      <div id="external-events">
                        <p style={{ alignSelf: "center" }}>
                          <strong> {t("Calendar.Orders")}</strong>
                        </p>
                        <List dataSource={todaysOrders}
                          renderItem={(item, index) => {
                            return <Banner
                              key={index}
                              orders={item}
                              onClick={() => openModal(
                                <OrderInfo
                                  orderId={item.id}
                                  onStatusChange={() => reloadOrers()} />
                              )} />;
                          }}
                        >
                        </List>
                      </div>
                    </Col>

                    <Col xl={18} md={18}>
                      <div className="demo-app-calendar" id="mycalendartest">
                        <FullCalendar
                          headerToolbar={{
                            left: "prev,next today",
                            center: "title",
                            right:
                              "dayGridMonth,timeGridWeek,timeGridDay,listGridWekk",

                          }}
                          buttonText={i18n.language === "hr" ? { today: "Danas", week: "Tjedan", month: "Mijesec", day: "Dan" } : { today: "Today", week: "Week", month: "Month", day: "Day" }}
                          locale={i18n.language}
                          eventOverlap={false}
                          rerenderDelay={10}
                          eventDurationEditable={false}
                          editable={true}
                          droppable={true}
                          plugins={[
                            dayGridPlugin,
                            timeGridPlugin,
                            interactionPlugin,
                          ]}
                          ref={ref}
                          events={eventList}
                          eventDrop={handleReschedule}
                          eventClick={(arg) =>
                            openModal(
                              <OrderInfo
                                orderId={arg.event.id}
                                onStatusChange={() => reloadOrers()}
                              />
                            )
                          }
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default observer(Main);
