import React from "react";
import { Star } from "react-feather";
import { useTranslation } from "react-i18next";
import { INotification } from "../../../models/notification";


interface Props {
  notification: INotification;
  onClick: () => void;
}
export const NotificationReview: React.FC<Props> = ({
  notification,
  onClick,
}) => {
  const { t } = useTranslation();

  return (
    <li onClick={onClick}>
      <div className="media">
        <div className="media-body">
          <h6 className="mt-0 txt-success">
            <span>
              <Star className="font-success" />
            </span>
            {t("Notification.Review")}
          </h6>
          <p className="mb-0">{t("Notification.ReviewMessage")}</p>
        </div>
      </div>
    </li>
  );
};
