import React from "react";
import { ServiceFormContext } from "../../View/ServiceForm/Context";

export function useServiceFormContext() {
  const sfContext = React.useContext(ServiceFormContext);
  return sfContext;
}
