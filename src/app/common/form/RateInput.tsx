import { FieldRenderProps } from "react-final-form";
import React from "react";
import { FormFieldProps, Form, Label, Rating } from "semantic-ui-react";

interface IProps
  extends FieldRenderProps<number, HTMLElement>,
    FormFieldProps {}

export const RateInput: React.FC<IProps> = ({
  input,
  width,
  defaultrating,
  maxrating,
  meta: { touched, error },
}) => {
  return (
    <Form.Field error={touched && !!error} width={width}>
      <Rating
        value={input.value}
        maxRating={maxrating}
        defaultRating={defaultrating}
      />
      {touched && error && (
        <Label basic color="red">
          {error}
        </Label>
      )}
    </Form.Field>
  );
};

export default RateInput;
