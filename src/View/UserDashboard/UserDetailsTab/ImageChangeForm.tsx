import { Button, Col, Form, Image as ImageAnt, Row, Upload } from "antd";
import { observer } from "mobx-react-lite";
import React, { useContext, useState } from "react";
import ImgCrop from "antd-img-crop";
import { RootStoreContext } from "../../../app/stores/rootStore";
import { RcFile } from "antd/lib/upload";
import { Inbox } from "react-feather";
import { UploadFile, UploadProps } from "antd/lib/upload/interface";
import { useTranslation } from "react-i18next";
import { useForm } from "antd/lib/form/Form";
import { IProfile } from "../../../app/models/profile";

interface ImageChangeFormProps {
    profile: IProfile;
}

const ImageChangeForm: React.FC<ImageChangeFormProps> = ({ profile }) => {
    const rootStore = useContext(RootStoreContext);
    const { uploadPhoto, loadProfile, uploadingPhoto } = rootStore.profileStore;
    const { getUser } = rootStore.userStore;
    const { closeModal } = rootStore.modalStore;
    const [blob, setBlob] = useState<RcFile>(null);
    const [imageForm] = useForm();
    const [imageSrc, setImageSrc] = useState<string>(profile?.image);
    const onPreview = async (file: object) => {
        setImageSrc(URL.createObjectURL(file));
    };
    const { t } = useTranslation();
    const uploadProps: UploadProps = {
        name: "blob",
        multiple: false,
        showUploadList: false,
        onRemove(file: UploadFile) {
            setBlob(null);
        },
        beforeUpload(file: RcFile) {
            setBlob(file);
            onPreview(file);
            return false;
        },
        accept: "image/jpeg, image/png, image/jpg, .jpeg , .jpg , .png",
    };

    const handleOnSave = () => {
        if (blob)
            uploadPhoto(blob!).then(() => {
                loadProfile(profile.username).then(() => {
                    getUser();
                    closeModal();
                })
            })
    }

    return (
        <div className="container">
            <Row gutter={16}>
                <Col span={24} style={{ marginBottom: 24 }}>
                    <Form form={imageForm} onFinish={handleOnSave}>
                        <ImgCrop aspect={1.206 / 1}>
                            <Upload.Dragger {...uploadProps}>
                                <div>
                                    <h2 className="mb-text-center mb-margin-bottom-xs mb-color-text-gray-secondary">
                                        <Inbox style={{ fontSize: "32px" }} />
                                    </h2>
                                    <h5 className="mb-text-center mb-margin-bottom-xxs">
                                        {t("ImageForm.ClickAndDrag")}
                                    </h5>
                                    <div className="mb-text-center mb-text-secondary-gray-color">
                                        {t("ImageForm.Description")}
                                    </div>
                                </div>
                            </Upload.Dragger>
                        </ImgCrop>
                    </Form>
                </Col>
                {imageSrc && (
                    <Col offset={2} span={20}>
                        <ImageAnt src={imageSrc} />
                    </Col>
                )}
                <Col span={24} style={{ marginTop: 12 }}>
                    <Row gutter={16}>
                        <Col span={4} offset={8}>
                            <Button block onClick={() => closeModal()}>
                                {t("ServiceWizard.Cancel")}
                            </Button>
                        </Col>
                        <Col span={4} >
                            <Button block type="primary" onClick={() => handleOnSave()} loading={uploadingPhoto}>
                                {t("ServiceWizard.Save")}
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
};

export default observer(ImageChangeForm);
