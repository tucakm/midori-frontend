import React from "react";
import { Route, RouteComponentProps, Switch } from "react-router-dom";
import NotFound from "../../app/layout/NotFound";
import { default as UserDashboardLayout } from "./Layout";

export const Routes: React.FC<RouteComponentProps> = ({ match }) => {
  return (
    <Switch>
      <Route path={match.path} component={UserDashboardLayout} />
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};
