import React, { Fragment, useContext, useState } from "react";
import logo from "../../../assets/images/endless-logo.png";
//import Language from './Language';
import Notification from "./Notifications/Notification";
import SearchHeader from "./SearchHeader";
import { Link, RouteComponentProps } from "react-router-dom";
import { AlignLeft, Bell } from "react-feather";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "../../stores/rootStore";
import UserMenu from "./UserMenu";
import Language from "./Language";
import { useTranslation } from "react-i18next";

const Header: React.FC<RouteComponentProps> = ({
  history,
  location,
  match,
}) => {
  const rootStore = useContext(RootStoreContext);
  const { isLoggedIn, user } = rootStore.userStore;
  const [language, setLanguage] = useState<string>("EN");
  const { t } = useTranslation();
  return (
    <Fragment>
      <div className="page-main-header" style={{ position: "fixed", top: 0, left: 0, zIndex: 999, width: "100%", backgroundColor: "white", marginBottom: "30px" }}>
        <div className="main-header-right row">
          <div className="main-header-left d-lg-none">
            <div className="logo-wrapper">
              <Link to="/dashboard/default">
                <img className="img-fluid" src={logo} alt="" />
              </Link>
            </div>
          </div>
          <div className="mobile-sidebar d-block">
            <div className="media-body text-right switch-sm">
              <label className="switch">
                <Link to="/">
                  <AlignLeft />
                </Link>
              </label>
            </div>
          </div>
          <div className="nav-right col p-0">
            <ul className={`nav-menus open`}>
              <li>
                <SearchHeader
                  history={history}
                  location={location}
                  match={match}
                />
              </li>
              <li className="onhover-dropdown">
                <a className="txt-dark" href="#javascript">
                  <h6>{language}</h6>
                </a>
                <Language
                  onLanguageChange={(language) => setLanguage(language)}
                />
              </li>
              {isLoggedIn && user ? (
                <Fragment>
                  <li className="onhover-dropdown">
                    <Notification />
                    <Bell />
                    <span className="dot"></span>
                    <Notification />
                  </li>
                  <UserMenu user={user} />
                </Fragment>
              ) : (
                <div className="btn-showcase">
                  <button
                    className="btn btn-pill btn-outline-primary btn-air-primary"
                    type="button"
                    onClick={(e) => {
                      e.preventDefault();
                      window.location.href = "/signin";
                    }}
                  >
                    {t("Header.SignIn")}
                  </button>
                  <button
                    className="btn btn-pill btn-primary btn-air-primary"
                    type="button"
                    onClick={(e) => {
                      e.preventDefault();
                      window.location.href = "/signup";
                    }}
                  >
                    {t("Header.SignUp")}
                  </button>
                </div>
              )}
            </ul>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default observer(Header);
