import { Alert } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import { DateDisplay } from "../../../app/common/date-display/DateDisplay";
import { IOrder } from "../../../app/models/order";

interface BannerProps {
  orders: IOrder;
  onClick?: (orderId: string) => void;
}

export const Banner: React.FC<BannerProps> = ({ orders, onClick }) => {
  const { t } = useTranslation();
  return (
    <div>
      <Alert
        showIcon
        message={orders.service.name}
        description={
          <>
            <div>
              {t("Calendar.Banner.Date")}{" "}
              <DateDisplay value={orders.date} format="DD/MM/YYYY" />
            </div>
            <div>
              {t("Calendar.Banner.StartTime")}{" "}
              <DateDisplay value={orders.startTime} format="HH" />
            </div>
            <div>
              {t("Calendar.Banner.EndTime")}{" "}
              <DateDisplay value={orders.endTime} format="HH" />
            </div>
          </>
        }
        onClick={() => onClick(orders.id)}
        type="info"
      ></Alert>
    </div>
  );
};
