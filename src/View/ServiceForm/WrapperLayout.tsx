import { observer } from "mobx-react-lite";
import React from "react";
import { ServiceFormContextProvider } from "./Context/ServiceFormContext";
import FormLayout from "./FormLayout";

// wrap layout with context
const WrapperLayout = () => {
  return (
    <ServiceFormContextProvider>
      <FormLayout />
    </ServiceFormContextProvider>
  );
};
export default observer(WrapperLayout);
