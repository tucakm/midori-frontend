import ModalStore from "./modalStore";
import { configure } from "mobx";
import { createContext } from "react";
import CommonStore from "./commonStore";
import UserStore from "./userStore";
import ServiceStore from "./serviceStore";
import ProfileStore from "./profileStore";
import OrderStore from "./orderStore";
import NotificationStore from "./notificationStore";

configure({ enforceActions: "always" });

export class RootStore {
  modalStore: ModalStore;
  commonStore: CommonStore;
  userStore: UserStore;
  serviceStore: ServiceStore;
  profileStore: ProfileStore;
  orderStore: OrderStore;
  notificationStore: NotificationStore;

  constructor() {
    this.serviceStore = new ServiceStore(this);
    this.modalStore = new ModalStore(this);
    this.commonStore = new CommonStore(this);
    this.userStore = new UserStore(this);
    this.profileStore = new ProfileStore(this);
    this.orderStore = new OrderStore(this);
    this.notificationStore = new NotificationStore(this);
  }
}
export const RootStoreContext = createContext(new RootStore());
