import { Col, Form, Input, Row, Select, Typography } from "antd";
import TextArea from "antd/lib/input/TextArea";
import React, { useContext, useState } from "react";
import { CurrencyInput } from "../../../app/common/currency/CurrencyInput";
import { LoadingComponent } from "../../../app/layout/LoadingComponent";
import { RootStoreContext } from "../../../app/stores/rootStore";
import { useServiceFormContext } from "../../../app/util/hooks";
import { observer } from "mobx-react-lite";
import { useHistory } from "react-router";
import { ICity, IServiceFormValues } from "../../../app/models/service";
import { useTranslation } from "react-i18next";
import { cityList } from "../../../app/common/cityList/cityList";

interface ServiceFormV2Props {
  onStepSwitch: (isSwitching: boolean) => void;
}
const { Option } = Select;
const ServiceFormV2: React.FC<ServiceFormV2Props> = ({ onStepSwitch }) => {
  const rootStore = useContext(RootStoreContext);
  const { createService } = rootStore.serviceStore;
  const serviceContext = useServiceFormContext();
  const { serviceModel, serviceForm, setServiceModel } = serviceContext;
  const history = useHistory();
  const [isSomethingChanged, setIsSomethingChanged] = useState<boolean>(false);
  const { t } = useTranslation();

  const handleSave = async () => {
    if (isSomethingChanged)
      serviceForm.validateFields().then(async (values: any) => {
        const serviceVar = { ...serviceModel, ...values };
        createService(serviceVar).then((response) => {
          if (response) {
            setServiceModel(response);
            history.replace(`/manage/${response.id}`);
            onStepSwitch(true);
          } else onStepSwitch(false);
        });
      });
    else onStepSwitch(true);
  };
  const handleChange = (
    diff: Partial<IServiceFormValues>,
    all: IServiceFormValues
  ) => {
    if (diff) setIsSomethingChanged(true);
    else setIsSomethingChanged(false);
  };

  if (!serviceModel) return <LoadingComponent />;
  return (
    <div className="container">
      <Row gutter={16}>
        <Col span={24}>
          <Form
            form={serviceForm}
            layout="vertical"
            name="service-form"
            scrollToFirstError
            initialValues={serviceModel}
            onFinish={handleSave}
            onValuesChange={handleChange}
            autoComplete="off"
            className="theme-form"
          >
            <Row gutter={[16, 16]}>
              <Col span={10}>
                <Form.Item
                  label={t("ServiceForm.LabelName")}
                  name="name"
                  rules={[
                    { required: true, message: t("ServiceForm.RuleName") },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label={t("ServiceForm.LabelIntroduction")}
                  name="introduction"
                  required
                  rules={[
                    {
                      required: true,
                      message: t("ServiceForm.RuleIntroduction"),
                    },
                  ]}
                >
                  <TextArea rows={3} />
                </Form.Item>
                <Form.Item
                  label={t("ServiceForm.LabelDescription")}
                  name="description"
                  rules={[
                    {
                      required: true,
                      message: t("ServiceForm.RuleDescription"),
                    },
                  ]}
                >
                  <TextArea rows={5} />
                </Form.Item>

                <Form.Item
                  name="city"
                  label={t("ServiceForm.City")}
                  rules={[
                    {
                      required: true,
                      message: t("ServiceForm.RuleCity"),
                    },
                  ]}
                >
                  <Select placeholder={t("ServiceForm.City")}>
                    {cityList.map((value: ICity, _i) => {
                      return (
                        <Option value={value.city} key={_i}>
                          {value.city}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                <Row>
                  <Col span={6} style={{ paddingTop: 35 }}>
                    <Typography.Text>
                      {t("ServiceForm.LabelCurrency")} : HRK
                    </Typography.Text>
                  </Col>
                  <Col span={18}>
                    <Form.Item
                      label={t("ServiceForm.LabelPrice")}
                      name="price"
                      labelAlign="right"
                      rules={[
                        { required: true, message: t("ServiceForm.RulePrice") },
                      ]}
                    >
                      <CurrencyInput min={"1"} currency="HRK" />
                    </Form.Item>
                  </Col>
                </Row>

                <Form.Item label={t("ServiceForm.LabelTags")} name="tags">
                  <Select
                    mode="tags"
                    tokenSeparators={[","]}
                    size="middle"
                    dropdownStyle={{ display: "none" }}
                    style={{ width: "100%" }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </div>
  );
};
export default observer(ServiceFormV2);
