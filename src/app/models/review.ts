import { Moment } from "moment";
import { IUser } from "./user";

export interface IReview {
  id: string;
  date: Moment;
  text: string;
  rate?: number;
  user: IUser;
}
