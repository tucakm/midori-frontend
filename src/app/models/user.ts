export interface IUser {
  username: string;
  email: string;
  token: string;
  image?: string;
  firstName?: string;
  lastName?: string;
  address?: string;
  bio?: string;
  job?: string;
}

export interface IUserFormValues {
  firstName?: string;
  lastName?: string;
  username?: string;
  email: string;
  password: string;
  birthday?: Date;
}

export interface IUserProfile {
  username?: string;
  email?: string;
  password?: string;
  image?: string;
  firstName?: string;
  lastName?: string;
  address?: string;
  bio?: string;
}
