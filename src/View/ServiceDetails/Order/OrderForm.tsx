import { PropertySafetyFilled } from "@ant-design/icons";
import {
  Button,
  Col,
  DatePicker,
  Form,
  Row,
  TimePicker,
  Typography,
} from "antd";
import { useForm } from "antd/lib/form/Form";
import TextArea from "antd/lib/input/TextArea";
import moment, { Moment } from "moment";
import React, { useCallback, useContext, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { currencyFormater } from "../../../app/common/currency/CurrencyInput";
import { IOrder } from "../../../app/models/order";
import { IService } from "../../../app/models/service";
import { RootStoreContext } from "../../../app/stores/rootStore";

interface OrderFormProps {
  service: IService;
  reload(): void;
}
export const OrderForm: React.FC<OrderFormProps> = ({
  service,
  reload
}: OrderFormProps) => {
  const [form] = useForm();
  const [timeDiff, setTime] =
    useState<{ startTime?: Moment; endTime?: Moment }>();
  const rootStore = useContext(RootStoreContext);
  const { closeModal } = rootStore.modalStore;
  const { orderService } = rootStore.orderStore;
  const { t } = useTranslation();
  const handleSave = () => {
    form.validateFields().then((fileds: IOrder) => {

      const { date, startTime, endTime } = fileds;
      var newDate = moment(date).startOf("day").utc(true);
      var newStarTime = moment(newDate).hour(startTime.hour()).utc(true);
      var newEndTime = moment(newDate).hour(endTime.hour()).utc(true);
      var modelForSave = {
        service: service,
        ...fileds,
        date: newDate,
        startTime: newStarTime,
        endTime: newEndTime,
      };

      orderService(modelForSave).then((response) => {
        if (response) {
          reload();
          closeModal();

        }
      });
    });
  };

  const handleOnChange = (diff: Partial<IOrder>, all: IOrder) => {
    setTime(all);
  };

  const handleStartTimeDisabled = useCallback(() => {
    var hours = timeDiff?.endTime?.hours();
    console.log(hours);
    var listHours = [];
    for (var i = 24; i >= hours; i--) {
      listHours.push(i);
    }
    return listHours;
  }, [timeDiff]);

  const handleEndTimeDisabled = useCallback(() => {
    var hours = timeDiff?.startTime?.hours();
    var listHours = [];
    for (var i = 0; i <= hours; i++) {
      listHours.push(i);
    }
    return listHours;
  }, [timeDiff]);

  const TotalPrice = useMemo(() => {
    if (timeDiff && timeDiff.startTime && timeDiff.endTime) {

      /*var hours = timeDiff.endTime
        .utc(true)
        .diff(timeDiff.startTime.utc(true), "hours");*/
      var hours = timeDiff.endTime.hours() - timeDiff.startTime.hours();
      var totalPrice = hours * service.price;
      return (
        <div>
          {t("OrderForm.EstimatedPrice")}
          <Typography.Text strong>
            {currencyFormater(totalPrice)}
          </Typography.Text>
        </div>
      );
    } else return null;
  }, [service, timeDiff, t]);

  const handleDisabledDate = useCallback((date: moment.Moment) => {
    return date.isBefore(moment.now());
  }, []);
  return (
    <div>
      <Form
        form={form}
        layout="vertical"
        onValuesChange={handleOnChange}
        onFinish={handleSave}
      >
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item
              name="date"
              label={t("OrderForm.FormDateLabel")}
              rules={[{ required: true, message: t("OrderForm.FromDateRule") }]}
            >
              <DatePicker
                style={{ width: "100%" }}
                format="DD/MM/YYYY"
                disabledDate={handleDisabledDate}
                showTime={false}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="startTime"
              label={t("OrderForm.FormStartTimeLabel")}
              rules={[
                { required: true, message: t("OrderForm.FromStartTimeRule") },
              ]}
            >
              <TimePicker
                style={{ width: "100%" }}
                format="HH"
                disabledHours={() => handleStartTimeDisabled()}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="endTime"
              label={t("OrderForm.FormEndTimeLabel")}
              rules={[
                { required: true, message: t("OrderForm.FromEndTimeRule") },
              ]}
            >
              <TimePicker
                style={{ width: "100%" }}
                format="HH"
                disabledHours={() => handleEndTimeDisabled()}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item name="comment" label={t("OrderForm.Comment")}>
              <TextArea rows={4} />
            </Form.Item>
          </Col>
          <Col span={24}>{TotalPrice}</Col>
        </Row>
        <Row gutter={16}>
          <Col span={4} offset={8}>
            <Button type="primary" block onClick={() => form.submit()}>
              {t("OrderForm.Order")}
            </Button>
          </Col>
          <Col span={4}>
            <Button type="primary" ghost block onClick={() => closeModal()}>
              {t("OrderForm.Cancel")}
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};
