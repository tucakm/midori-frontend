import { RootStore } from "./rootStore";
import { observable, action, runInAction, computed } from "mobx";
import { IService } from "../models/service";
import { ITag } from "../models/tag";
import agent from "../api/agent";
import {
  HubConnection,
  HubConnectionBuilder,
  LogLevel,
} from "@microsoft/signalr";
import { toast } from "react-toastify";
import { computedFn } from "mobx-utils";
import { IReview } from "../models/review";

export const LIMIT = 8;

export default class ServiceStore {
  rootStore: RootStore;

  constructor(RootStore: RootStore) {
    this.rootStore = RootStore;
  }

  @observable servicesRegisty = new Map();
  @observable service: IService | null = null;
  @observable selectedService: IService | null = null;
  @observable loadingInitial = true;
  @observable submiting = false;
  @observable sorting = false;
  @observable target = "";
  @observable loading = false;
  @observable.ref hubConnection: HubConnection | null = null;

  @observable servicesForUser = new Map();
  @observable loadingServices = false;

  @observable loadingTags = false;
  @observable tagsRegisty = new Map();

  @observable uploadingPhoto = false;
  @observable filteredServicesRegistry = new Map();

  @observable thisServiceTags = new Map();

  @observable serviceCount: number = 0;
  @observable page: number = 0;

  @computed get totalPage() {
    return Math.ceil(this.serviceCount / LIMIT);
  }

  @action setPage = (page: number) => {
    this.page = page;
  };

  @action createHubConnection = (serviceId: string) => {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl("http://localhost:5000/chat", {
        accessTokenFactory: () => this.rootStore.commonStore.token!,
      })
      .configureLogging(LogLevel.Information)
      .build();

    this.hubConnection
      .start()
      .then(() => console.log(this.hubConnection!.state))
      .then(() => {
        console.log("Attempting to join group");
        this.hubConnection!.invoke("AddToGroup", serviceId);
      })
      .catch((error) => console.log("Error establishing connection:", error));

    this.hubConnection.on("ReceiveReview", (review) => {
      runInAction(() => {
        this.service!.review.push(review);
      });
    });

    this.hubConnection.on("Send", (message) => {
      toast.info(message);
    });
  };
  @action stopHubConnection = () => {
    this.hubConnection
      ?.invoke("RemoveFromGroup", this.service!.id)
      .then(() => {
        this.hubConnection!.stop();
      })
      .then(() => console.log("Connection stoped"))
      .catch((err) => console.log(err));
  };

  @action addReview = async (values: any) => {
    values.serviceId = this.service!.id;

    try {
      await this.hubConnection!.invoke("SendReview", values);
    } catch (error) {
      console.log(error);
    }
  };

  @action createReview = async (values: IReview, serviceId: string) => {
    try {
      var response = await agent.Services.addReview(values, serviceId);

      if (response.id) toast.success(`Succesfuly left review`);
    } catch (error) {
      console.log(error);
      toast.success(`Some error happened review`);
    }
  };
  filterServices = computedFn((criteria: string) => {
    return Array.from(this.servicesRegisty.values())
  });


  filterTags = (tags: ITag[]) => {
    return tags.filter(function (item, index, self) {
      return self.findIndex(x => x.tagName === item.tagName) === index
    })
  }
  @computed get serviceArray() {
    return Array.from(this.servicesRegisty.values());
  }
  @computed get tagsArray() {
    return this.filterTags(Array.from(this.tagsRegisty.values()));
  }
  @computed get serviceTagsArray() {
    return Array.from(this.thisServiceTags.values());
  }
  @action loadServices = async (criteria: string, sort?: string) => {
    if (sort) this.sorting = true;
    else this.loadingInitial = true;
    if (this.page === 0) this.servicesRegisty.clear();
    try {
      const serviceWrapper = await agent.Services.list(LIMIT, this.page, criteria, null, sort);
      const { services, serviceCount } = serviceWrapper;
      runInAction(() => {
        services.forEach((service) => {
          this.servicesRegisty.set(service.id, service);
        });
        this.serviceCount = serviceCount;
        if (sort) this.sorting = false;
        else this.loadingInitial = false;
      });
    } catch (error) {
      runInAction(() => {
        if (sort) this.sorting = false;
        else this.loadingInitial = false;
      });
    }
  };

  @action loadService = async (id: string): Promise<IService> => {
    let service = this.getService(id);
    if (service) {
      this.getMappedTagsForService(service);
      this.service = service;
      this.loadingInitial = false;
      return service;
    } else {
      this.loadingInitial = true;
      try {
        service = await agent.Services.details(id);
        runInAction(() => {
          this.service = service;
          this.servicesRegisty.set(service.id, service);
          this.getMappedTagsForService(service);

          this.loadingInitial = false;
        });
        return this.service;
      } catch (error) {
        runInAction(() => {
          this.loadingInitial = false;
        });
      }
    }
  };
  getMappedTagsForService(service: IService) {
    if (service!.serviceTags && service!.serviceTags?.length > 0) {
      service?.serviceTags?.map((tag: ITag) => {
        this.thisServiceTags.set(tag.tagId, tag.tagName);
      });
    } else {
      this.thisServiceTags.clear();
    }
  }
  @action createService = async (service: IService): Promise<IService> => {
    this.submiting = true;
    try {
      service.user = this.rootStore.userStore.user;
      service.price = parseFloat(`${service.price}`);
      var response = await agent.Services.create(service);

      response.review = [];
      runInAction(() => {
        this.service = response;
        this.servicesRegisty.set(service.id, service);
        this.submiting = false;
      });
      if (service.id) toast.success(`Service updated succesfuly`);
      else toast.success(`Service created succesfuly`);
      return response;
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      console.log(error);
      toast.error("Problem submitting data");
    }
  };

  @action changeStatus = async (
    serviceId: string,
    status: number
  ): Promise<IService> => {
    this.submiting = true;
    try {
      var response = await agent.Services.changeStatus(serviceId, status);
      runInAction(() => {
        this.service = response;
        this.servicesRegisty.set(response.id, response);
        this.submiting = false;
      });
      toast.success(`Service status changed`);
      return response;
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      console.log(error);
      toast.error("Problem submitting data");
    }
  };
  @action selectService = (id: string) => {
    this.selectedService = this.servicesRegisty.get(id);
  };

  @action deleteService = async (service: IService) => {
    this.submiting = true;
    try {
      await agent.Services.delete(service.id);
      runInAction(() => {
        this.servicesRegisty.delete(service.id);
        this.submiting = false;
        this.target = "";
      });
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
        this.target = "";
      });
      console.log(error);
    }
  };

  getService = (id: string) => {
    return this.servicesRegisty.get(id);
  };

  @action loadTags = async () => {
    this.loadingTags = true;
    try {
      const tags = await agent.Services.listTags();
      runInAction(() => {
        tags.forEach((tag) => {
          this.tagsRegisty.set(tag.tagId, tag);
        });
        this.loadingTags = false;
      });
    } catch (error) {
      runInAction(() => {
        console.log(error);
        this.loadingTags = false;
      });
    }
  };

  @action uploadPhoto = async (file: Blob): Promise<IService> => {
    this.uploadingPhoto = true;
    try {
      const photo = await agent.Services.uploadPhotoForService(
        file,
        this.service!.id
      );
      runInAction(() => {
        if (this.service) {
          this.service.image = photo.url;
        }
        this.uploadingPhoto = false;
      });
      toast.success("Photo updated succesfuly");
      return this.service;
    } catch (error) {
      console.log(error);
      toast.error("Problem uploading photo");
      runInAction(() => {
        this.uploadingPhoto = false;
      });
    }
  };
  @action createTagForService = async (serviceId: string, Tags: string[]) => {
    this.loadingInitial = true;

    try {
      const tags = await agent.Services.createTagForService(serviceId, Tags);
      runInAction(() => {
        this.loadingInitial = false;
        tags.map((tag: ITag) => {
          return this.thisServiceTags.set(tag.tagId, tag.tagName);
        });
      });
    } catch (error) {
      console.log(error);
      toast.error("Problem adding tags");
      runInAction(() => {
        this.loadingInitial = false;
      });
    }
  };
  @action loadUserServices = async (username: string): Promise<IService[]> => {
    this.loadingInitial = true;
    try {
      const services = await agent.Services.listUserServices(username);
      runInAction(() => {
        this.loadingInitial = false;
      });
      return services;
    } catch (error) {
      console.log(error);
      toast.error("Problem loading user services ");
      runInAction(() => {
        this.loadingInitial = false;
      });
    }
  };
  @action clearService = async () => {
    this.service = null;
  }
}
