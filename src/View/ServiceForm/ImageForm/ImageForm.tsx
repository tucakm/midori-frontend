import { Col, Form, Image as ImageAnt, Row, Upload } from "antd";
import { observer } from "mobx-react-lite";
import React, { useContext, useState } from "react";
import ImgCrop from "antd-img-crop";
import { RootStoreContext } from "../../../app/stores/rootStore";
import { useServiceFormContext } from "../../../app/util/hooks";
import { RcFile } from "antd/lib/upload";
import { Inbox } from "react-feather";
import { UploadFile, UploadProps } from "antd/lib/upload/interface";
import { useTranslation } from "react-i18next";

interface ImageFormProps {
  onStepSwitch: (isSwitching: boolean) => void;
}

const ImagesForm: React.FC<ImageFormProps> = (props) => {
  const rootStore = useContext(RootStoreContext);
  const { uploadPhoto } = rootStore.serviceStore;
  const serviceContext = useServiceFormContext();
  const { serviceModel, imageForm, setServiceModel } = serviceContext;
  const [blob, setBlob] = useState<RcFile>(null);
  const [imageSrc, setImageSrc] = useState<string>(serviceModel.image);
  const onPreview = async (file: object) => {
    setImageSrc(URL.createObjectURL(file));
  };
  const { t } = useTranslation();
  const uploadProps: UploadProps = {
    name: "blob",
    multiple: false,
    showUploadList: false,
    onRemove(file: UploadFile) {
      setBlob(null);
    },
    beforeUpload(file: RcFile) {
      setBlob(file);
      onPreview(file);
      return false;
    },
    accept: "image/jpeg, image/png, image/jpg, .jpeg , .jpg , .png",
  };

  const handleOnSave = () => {
    if (blob)
      uploadPhoto(blob!).then((resposne) => {
        if (resposne) {
          setServiceModel(resposne);
          props.onStepSwitch(true);
        } else {
          props.onStepSwitch(false);
        }
      });
    else props.onStepSwitch(true);
  };
  return (
    <div className="container">
      <Row gutter={16}>
        <Col span={24} style={{ marginBottom: 24 }}>
          <Form form={imageForm} onFinish={handleOnSave}>
            <ImgCrop aspect={1.206 / 1}>
              <Upload.Dragger {...uploadProps}>
                <div>
                  <h2 className="mb-text-center mb-margin-bottom-xs mb-color-text-gray-secondary">
                    <Inbox style={{ fontSize: "32px" }} />
                  </h2>
                  <h5 className="mb-text-center mb-margin-bottom-xxs">
                    {t("ImageForm.ClickAndDrag")}
                  </h5>
                  <div className="mb-text-center mb-text-secondary-gray-color">
                    {t("ImageForm.Description")}
                  </div>
                </div>
              </Upload.Dragger>
            </ImgCrop>
          </Form>
        </Col>
        {imageSrc && (
          <Col offset={2} span={20}>
            <ImageAnt src={imageSrc} />
          </Col>
        )}
      </Row>
    </div>
  );
};

export default observer(ImagesForm);
