import { FormInstance } from "antd";
import { useForm } from "antd/lib/form/Form";
import React, { PropsWithChildren, useCallback, useState } from "react";
import { IServiceFormValues } from "../../../app/models/service";

export interface ServiceFormContextState {
  currentStep: number;
  serviceModel: IServiceFormValues | null;
  handleNextButtonClick: () => void;
  handlePreviousClick: () => void;
  setServiceModel: (serivce: IServiceFormValues) => void;
  serviceForm: FormInstance;
  stepSwitching: (isStepSwitchValid: boolean) => void;
  imageForm: FormInstance;
}

export interface ServiceFormContractStateInit extends ServiceFormContextState { }
interface ServiceFormContextProps { }

export const ServiceFormContext =
  React.createContext<ServiceFormContextState>(null);

export const ServiceFormContextProvider: React.FC<
  PropsWithChildren<ServiceFormContextProps>
> = ({ children }) => {
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [nextStep, setNextStep] = useState<number>(0);
  const [serviceFormModel, setServiceFormModel] =
    useState<IServiceFormValues>(null);

  const [serviceForm] = useForm();
  const [imageForm] = useForm();
  const StepSwitching = useCallback(
    (isStepSwitchValid: Boolean) => {
      if (isStepSwitchValid) {
        console.log(nextStep)
        setCurrentStep(nextStep);
        setNextStep(0);
      }
    },
    [nextStep]
  );

  const handleNextButtonClick = () => {
    if (currentStep === 0) {
      serviceForm.submit();
      setNextStep((currentStep) => {
        return currentStep + 1;
      });
    } else if (currentStep === 1) {
      imageForm.submit();
      setNextStep(() => {
        return currentStep + 1;
      });
    }
  };
  const handlePreviousClick = () => {
    setCurrentStep((currentStep) => {
      return currentStep - 1;
    });
  };
  const setServiceModel = useCallback((service: IServiceFormValues) => {
    setServiceFormModel(service);
  }, []);

  return (
    <ServiceFormContext.Provider
      value={{
        currentStep: currentStep,
        serviceModel: serviceFormModel,
        handleNextButtonClick: handleNextButtonClick,
        handlePreviousClick: handlePreviousClick,
        setServiceModel: setServiceModel,
        serviceForm: serviceForm,
        stepSwitching: StepSwitching,
        imageForm: imageForm
      }}
    >
      {children}
    </ServiceFormContext.Provider>
  );
};
