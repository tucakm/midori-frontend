import React from "react";
import { Segment, Grid, Container, Header } from "semantic-ui-react";
import { observer } from "mobx-react-lite";
import { RouteComponentProps } from "react-router-dom";

const HomePage: React.FC<RouteComponentProps> = () => {
  return (
    <Segment inverted textAlign="center" vertical className="masthead" style={{ paddingTop: "1em" }}>
      <Container className="home-page" >
        <Grid columns={2} relaxed="very" stackable>
          <Grid.Column></Grid.Column>
          <Grid.Column verticalAlign="middle">
            <Header as="h1" inverted>
              JobQuest
            </Header>
          </Grid.Column>
        </Grid>
      </Container>
    </Segment>
  );
};
export default observer(HomePage);
