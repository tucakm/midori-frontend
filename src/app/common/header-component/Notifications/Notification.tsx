import { observer } from "mobx-react-lite";
import React, { Fragment, useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { INotification, NotificationType } from "../../../models/notification";
import { RootStoreContext } from "../../../stores/rootStore";
import { NotificationNewOrder } from "./NotificationNewOrder";
import { NotificationNewReschedule } from "./NotificationNewReschedule";
import { NotificationReview } from "./NotificationReview";
import NotificationReviewModal from "./NotificationReviewModal";
import { RescheduleInfoModal } from "./RescheduleInfoModal";

const Notification = () => {
  const rootStore = useContext(RootStoreContext);
  const { loadNotifications, ordersArray } = rootStore.notificationStore;
  const { openModal } = rootStore.modalStore;
  const { t } = useTranslation();
  useEffect(() => {
    loadNotifications();
  }, [loadNotifications]);
  if (!ordersArray) return null;

  const handleNotifications = (notification: INotification): JSX.Element => {
    if (notification.type === NotificationType.NewOrder)
      return <NotificationNewOrder key={notification.id} notification={notification} />;
    else if (notification.type === NotificationType.RescheduleOrder)
      return (
        <NotificationNewReschedule
          key={notification.id}
          notification={notification}
          onClick={() =>
            openModal(<RescheduleInfoModal notification={notification} />)
          }
        />
      );
    else
      return (
        <NotificationReview
          notification={notification}
          key={notification.id}
          onClick={() => {
            openModal(<NotificationReviewModal notification={notification} />);
          }}
        />
      );
  };
  return (
    <Fragment>
      <div>
        <ul className="notification-dropdown onhover-show-div p-0">
          <li>
            {t("Notification.Title")}{" "}
            <span className="badge badge-pill badge-primary pull-right">
              {ordersArray.length}
            </span>
          </li>
          {ordersArray.map((notification: INotification) => {
            return handleNotifications(notification);
          })}
        </ul>
      </div>
    </Fragment>
  );
};

export default observer(Notification);
