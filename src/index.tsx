import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import "./app/layout/styles.scss";
import App from "./app/layout/App";
import * as serviceWorker from "./serviceWorker";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import "react-toastify/dist/ReactToastify.min.css";
import "react-widgets/dist/css/react-widgets.css";

import "@pathofdev/react-tag-input/build/index.css";
import "antd/dist/antd.css";
import "./i18n";
import { LoadingComponent } from "./app/layout/LoadingComponent";

export const history = createBrowserHistory();

ReactDOM.render(
  <Router history={history}>
    <Suspense fallback={<LoadingComponent content="Loading..." />}>
      <App />
    </Suspense>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
