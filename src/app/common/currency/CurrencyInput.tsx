import { InputNumber, InputNumberProps } from "antd";
import React, { useRef, useCallback, useEffect } from "react";
import { supportedCurrency } from "./currency";

interface CurrencyProps extends InputNumberProps<string> {
  currency: string;
}

//Used also inside grid to format values
export function currencyFormater(
  value: number | string,
  currency: string = "HRK",
  userLoacale?: string
): string {
  var locale = userLoacale ?? "hr-HR";
  var formater = Intl.NumberFormat(locale, currencyOptions(currency));
  if (value) return formater.format(Number.parseFloat(value.toString()));

  return value.toString();
}
function currencyOptions(currency: string): Intl.NumberFormatOptions {
  const currencySupproted =
    supportedCurrency.find((x) => x === currency) ?? null;
  var options: Intl.NumberFormatOptions = currencySupproted
    ? {
      style: "currency",
      currency: currencySupproted,
      currencyDisplay: "symbol",
    }
    : { style: "decimal" };

  return options;
}

export const CurrencyInput = (props: CurrencyProps) => {
  const locale = "hr-HR";
  const numberFormat = Intl.NumberFormat(
    locale,
    currencyOptions(props.currency)
  );
  //decimal separator
  const decimalSeparator = numberFormat
    .formatToParts(1000.1)
    .find((x) => x.type === "decimal")!.value!;

  const ref = useRef<HTMLInputElement>(null);

  const handleKeyUp = useCallback(
    (event: any) => {
      var value = event.target.value;
      var regex = new RegExp("[^0-9\\" + decimalSeparator + "]*", "g");
      //we do this so we can just have if statement for value
      var numberValue = value;
      numberValue = numberValue.replace(regex, "").replace(",", ".");
      //first input can be value from 0-9 if its not then its edit of value
      if (numberValue && numberValue < 10) {
        //find first digit in value eg $ 12232
        var firstDigit = value.match(/\d/);
        //get position of value
        var valuePosition = value.indexOf(firstDigit);
        //get cursor
        var cursor = event.target.selectionStart;
        if (event.target.selectionStart > valuePosition) {
          cursor = valuePosition + 1;
          event.target.setSelectionRange(cursor, cursor);
        }
        //ref.current.removeEventListener("keyup", handleKeyUp);
      }
    },
    [decimalSeparator]
  );

  useEffect(() => {
    const current = ref!.current!;
    current.addEventListener("keyup", handleKeyUp);
    return () => {
      current.removeEventListener("keyup", handleKeyUp);
    };
  }, [handleKeyUp]);

  const currencyParser = (value: string, decimalSeparator: string): string => {
    var regex = new RegExp("[^0-9\\" + decimalSeparator + "]*", "g");
    return value.replace(regex, "").replace(",", ".");
  };
  return (
    <InputNumber<string>
      ref={ref}
      style={{ width: "100%" }}
      formatter={(value) => currencyFormater(value!, props.currency, locale)}
      parser={(value) => currencyParser(value!, decimalSeparator)}
      stringMode
      {...props}
    />
  );
};
