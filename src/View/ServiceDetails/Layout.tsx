import { Alert, Button, Col, Rate, Row, Tag, Tooltip } from "antd";
import { observer } from "mobx-react-lite";
import React, { Fragment, useEffect, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import { currencyFormater } from "../../app/common/currency/CurrencyInput";
import { DateDisplay } from "../../app/common/date-display/DateDisplay";
import { LoadingComponent } from "../../app/layout/LoadingComponent";
import { IService, ServiceStatus } from "../../app/models/service";
import { ITag } from "../../app/models/tag";
import { RootStoreContext } from "../../app/stores/rootStore";
import { OrderForm } from "./Order/OrderForm";
import ReviewLayout from "./Reviews/ReviewLayout";

export const Layout: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { service, loadService, loadingInitial, clearService } = rootStore.serviceStore;
  const { openModal } = rootStore.modalStore;
  const { checkOrder } = rootStore.userStore;
  const { user } = rootStore.userStore;
  const { t } = useTranslation();
  const params = useParams<{ id: string }>();
  const [serviceModel, setServiceModel] = useState<IService | null>();
  const [hasOrder, setHasOrder] = useState<boolean>();

  useEffect(() => {
    if (params.id)
      loadService(params.id).then(() => {
        setServiceModel(service);
      }).then(() => checkOrder(params.id).then(response => { setHasOrder(response) }));

    return () => clearService();
  }, [params.id, loadService, service, checkOrder, clearService]);

  if (loadingInitial || !serviceModel)
    return <LoadingComponent content={t("LoadingIndicators.LoadingService")} />;

  if (!serviceModel) return <h2>{t("ServiceDetails.ServiceNotFound")}</h2>;
  const handleAlert = (status: ServiceStatus): JSX.Element => {
    if (status === ServiceStatus.Draft)
      return <Alert type="warning" message={t("ServiceDetails.Alert.Draft")} />;
    else if (status === ServiceStatus.Inactive)
      return <Alert type="error" message={t("ServiceDetails.Alert.Error")} />;
  };
  return (
    <Fragment>
      <div className="container-fluid" style={{ paddingTop: "90px" }}>
        <div className="card">
          {serviceModel.status !== ServiceStatus.Active &&
            handleAlert(serviceModel.status)}
          <div className="row product-page-main">
            <div className="col-xl-4">
              <div className="blog-single">
                <div className="blog-box blog-details">
                  <Slider arrows={true} className="product-slider">
                    {serviceModel.images ? (
                      serviceModel.images.map((item, i) => {
                        return (
                          <div className="item" key={i}>
                            <img
                              src={item}
                              alt="This is cool"
                              className="img-fluid"
                            />
                          </div>
                        );
                      })
                    ) : (
                      <img
                        src={
                          serviceModel.image ?? "/assets/imageplaceholder.png"
                        }
                        className="img-fluid"
                        alt="This is cool"
                      />
                    )}
                  </Slider>
                  <div className="blog-details">
                    <ul className="blog-social">
                      <li className="digits">
                        <DateDisplay
                          value={serviceModel.createdDate}
                          format="DD/MM/YYYY"
                        />
                      </li>
                      <li>
                        <i className="icofont icofont-user"></i>
                        <Link to={`/profile/${serviceModel.user.username}`}>
                          {serviceModel.user.username}
                        </Link>
                      </li>
                      <li className="digits">
                        <i className="icofont icofont-ui-chat"></i>
                        {serviceModel.review.length}{" "}
                        {t("ServiceDetails.Reviews")}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-8">
              <div className="product-page-details">
                <h4>{serviceModel.name}</h4>
                <div className="d-flex">
                  <Rate
                    defaultValue={serviceModel.rating}
                    allowHalf={true}
                    disabled={true}
                    style={{ color: "#0000ff" }}
                  />
                  <span>{`(${serviceModel.review.length}) ${t(
                    "ServiceDetails.LowerCaseReviews"
                  )}`}</span>
                </div>
              </div>
              <hr />
              <h6> {t("ServiceDetails.Description")}</h6>
              <p>{serviceModel.description}</p>
              <h6>{t("ServiceDetails.Price")}</h6>
              <div className="product-price digits">
                {currencyFormater(serviceModel.price)}{" "}
                {t("ServiceDetails.PerH")}
              </div>
              <hr />
              <div className="product-page-width">
                <Row gutter={16}>
                  <Col span="auto">
                    {t("ServiceDetails.Location")} : {serviceModel.city}
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col flex="none">{t("ServiceDetails.Tags")} :</Col>
                  <Col flex="auto">
                    {serviceModel.serviceTags?.map((item: ITag, i) => {
                      return <Tag key={item.tagId}>{item.tagName}</Tag>;
                    })}
                  </Col>
                </Row>
              </div>
              <hr />
              <div className="m-t-15">
                <Row gutter={16}>
                  {user && user.username && (
                    <Col span={4}>
                      <Tooltip title={serviceModel.user.username === user.username ?
                        t("ServiceDetails.TooltipOwner") : hasOrder ?
                          t("ServiceDetails.TooltipAllredyHasOrder") : ""}>
                        <Button
                          type="primary"
                          block
                          onClick={() =>
                            openModal(<OrderForm service={service} reload={() => loadService(params.id).then(() => {
                              setServiceModel(service);
                            }).then(() => checkOrder(params.id).then(response => { setHasOrder(response) }))} />)
                          }
                          disabled={serviceModel.user.username === user.username || hasOrder}
                        >
                          {t("ServiceDetails.Order")}
                        </Button>
                      </Tooltip>
                    </Col>
                  )}
                  <Col span={4}>
                    <a href={`mailto:${serviceModel.user.email}`}>
                      <Button type="primary" ghost block>
                        {t("ServiceDetails.Questions")}
                      </Button>
                    </a>
                  </Col>
                </Row>
              </div>
            </div>
            <ReviewLayout />
          </div>
        </div>
      </div>
    </Fragment >
  );
};
export default observer(Layout);
