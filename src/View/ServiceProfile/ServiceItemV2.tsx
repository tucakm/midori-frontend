import React from "react";
import { observer } from "mobx-react-lite";
import { IService } from "../../app/models/service";
import { Link, useHistory } from "react-router-dom";
import { DateDisplay } from "../../app/common/date-display/DateDisplay";
import { Rate } from "antd";
import { currencyFormater } from "../../app/common/currency/CurrencyInput";
import { useTranslation } from "react-i18next";



interface IProps {
  service: IService;
  editing?: boolean;
  onDelete?: () => void;
  onEdit?: () => void;
  loading?: boolean;
}

const ServiceItemV2: React.FC<IProps> = ({
  service,
  editing,
  onDelete,
  onEdit,
  loading
}) => {
  const history = useHistory();
  const { t } = useTranslation();
  const handleOnEyeClick = () => {
    history.push(`/service/${service.id}`);
  };
  if (service === null) return null;
  return (
    <div className="card features-faq product-box">
      <div className="blog-box product-box">

        <div className="product-img">
          <img
            className="img-fluid"
            src={service.image || "/assets/imageplaceholder.png"}
            alt=""
          />
          <div className="product-hover">
            <ul>
              {editing && (
                <li style={{ color: "blue" }}>
                  <button
                    className="btn"
                    type="button"
                    onClick={() => onEdit()}
                    data-target="#exampleModalCenter"
                  >
                    <i className="icofont icofont-ui-edit"></i>
                  </button>
                </li>
              )}
              <li>
                <button
                  className="btn"
                  type="button"
                  data-toggle="modal"
                  onClick={() => handleOnEyeClick()}
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-eye"></i>
                </button>
              </li>
              {editing && (
                <li style={{ color: "red" }}>
                  <button
                    className="btn"
                    type="button"
                    onClick={() => onDelete()}
                    data-target="#exampleModalCenter"
                  >
                    <i className="icofont icofont-trash"></i>
                  </button>
                </li>
              )}
            </ul>
          </div>
        </div>

        <div className="card-body">
          <div className="blog-details-main text-center">
            <ul className="blog-social">
              <li className="digits">
                <DateDisplay value={service.createdDate} format="DD/MM/YYYY" />
              </li>
              <li className="digits">
                {t("ServiceItem.By")}
                <Link to={`/profile/${service.user.username}`}>
                  {service.user.username}
                </Link>
              </li>
              <li className="digits">
                {t("ServiceItem.Location")} {service.city}
              </li>
            </ul>
            <hr />
          </div>
          <h6>
            <Link to={`service/${service.id}`}>{service.name}</Link>
          </h6>
          <p>
            {service.introduction.length > 150
              ? `${service.introduction.substring(0, 150)}...`
              : service.introduction}
          </p>
        </div>
        <div className="card-footer">
          <span className="">
            <Rate
              defaultValue={service.rating}
              allowHalf={true}
              disabled={true}
              style={{ color: "#0000ff", fontSize: 14 }}
            />
          </span>
          <span className="product-price pull-right">
            {currencyFormater(service.price)}
          </span>
        </div>
      </div>
    </div>
  );
};

export default observer(ServiceItemV2);
