import { Rate } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";
import { Link } from "react-router-dom";
import { DateDisplay } from "../../../app/common/date-display/DateDisplay";
import { IReview } from "../../../app/models/review";

interface ReviewItemProps {
  review: IReview;
}

const ReviewItem: React.FC<ReviewItemProps> = ({ review }) => {
  return (
    <div className="media align-self-center">
      <Link to={`/profile/${review.user.username}`}>
        <img
          className="align-self-center"
          src={review.user.image ?? "/assets/imageplaceholder.png"}
          alt="Generic placeholder"
        />
      </Link>
      <div className="media-body">
        <div className="row">
          <div className="col-md-3">
            <h6 className="mt-0">
              <Link to={`/profile/${review.user.username}`}>
                {review.user.username}
              </Link>
            </h6>
          </div>
          <div className="col-md-9">
            <ul className="comment-social float-left float-md-right">
              <li className="digits">
                <Rate
                  defaultValue={review.rate}
                  character={<i className="fa fa-star"></i>}
                  disabled
                  style={{ fontSize: 16 }}
                />
              </li>
              <li className="digits">
                <i className="icon-calendar"></i>
                <DateDisplay value={review.date} format={"DD/MM/YYYY"} />
              </li>
            </ul>
          </div>
        </div>
        <p>{review.text}</p>
      </div>
    </div>
  );
};

export default observer(ReviewItem);
