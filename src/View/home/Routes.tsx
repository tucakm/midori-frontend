import React from "react";
import { Route, RouteComponentProps, Switch } from "react-router-dom";
import NotFound from "../../app/layout/NotFound";
import HomePage from "./HomePage";

export const Routes: React.FC<RouteComponentProps> = ({ match }) => {
  return (
    <Switch>
      <Route path={match.path} exact component={HomePage} />
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};
