import React, {
  useState,
  Fragment,
  useEffect,
  useCallback,
  useContext,
  useRef,
  FormEvent,
} from "react";
import { RouteComponentProps } from "react-router-dom";
import { Search } from "react-feather";
import { ITag } from "../../models/tag";
import { RootStoreContext } from "../../stores/rootStore";
import { observer } from "mobx-react-lite";
import { useTranslation } from "react-i18next";

const SearchHeader: React.FC<RouteComponentProps> = ({ location, history }) => {
  const [searchValue, setsearchValue] = useState("");
  const [searchOpen, setsearchOpen] = useState(false);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const rootState = useContext(RootStoreContext);
  const { loadTags, tagsArray, loadingTags, setPage } = rootState.serviceStore;
  const searchParams = new URLSearchParams(location.search);
  const criteria = searchParams.get("criteria") || "";
  const [search, setSearch] = useState(criteria);
  const { t } = useTranslation();
  const escFunction = useCallback((event) => {
    if (event.keyCode === 27) {
      //Do whatever when esc is pressed
      setsearchOpen(false);
      setsearchValue("");
    }
  }, []);

  useEffect(() => {
    loadTags();
  }, [loadTags]);

  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });
  const handleClickOutside = (event: any) => {
    const { current: wrap } = wrapperRef;

    if (wrap && !wrap!.contains(event?.target)) {
      removeFix();
    }
  };

  const handleSearchKeyword = (keyword: any) => {
    keyword ? addFix() : removeFix();
    setsearchValue(keyword);
    setSearch(keyword);
    var items = [];
    // eslint-disable-next-line
    tagsArray!.filter((menuItems) => {
      if (
        menuItems.tagName.toLowerCase().indexOf(keyword.toLocaleLowerCase()) >
        -1
      ) {
        items.push(menuItems);
      }
      checkSearchResultEmpty(items);
      //setOptions(items);
    });
  };

  const checkSearchResultEmpty = (items: any) => {
    if (!items.length) {
      document.querySelector(".Typeahead-menu")!.classList.remove("is-open");
    } else {
      document.querySelector(".Typeahead-menu")!.classList.add("is-open");
    }
  };

  const addFix = () => {
    document.querySelector(".Typeahead-menu")!.classList.add("is-open");
    document.body.classList.add("offcanvas");
  };

  const removeFix = () => {
    setsearchValue("");
    document!.querySelector(".Typeahead-menu")!.classList.remove("is-open");
    document.body.classList.remove("offcanvas");
  };

  const toggleBtn = () => {
    if (searchOpen) {
      setsearchOpen(!searchOpen);
      document!.querySelector(".searchIcon")!.classList.add("open");
    } else {
      setsearchOpen(!searchOpen);
      document!.querySelector(".searchIcon")!.classList.remove("open");
    }
  };

  const handleSearchSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    searchValues(search);
  };
  const searchSelectedValues = (value?: string) => {
    if (value != null) {
      setSearch(value);
    }
    searchValues(value);
  };
  const searchValues = (searchParameters: any) => {
    setPage(0);
    history.push(`/search?criteria=${searchParameters}`);
    removeFix();
  };
  return (
    <Fragment>
      <div>
        <form className="form-inline search-form" onSubmit={handleSearchSubmit}>
          <div className="form-group">
            <input
              className="form-control-plaintext searchIcon"
              type="text"
              placeholder={t("Search.Placeholder")}
              onChange={(e) => handleSearchKeyword(e.target.value)}
              disabled={loadingTags}
              value={search}
            />
            <span className="d-sm-none mobile-search" onClick={toggleBtn}>
              <Search />
            </span>

            <div
              className="Typeahead-menu custom-scrollbar"
              id="search-outer"
              ref={wrapperRef}
            >
              {searchValue
                ? tagsArray!
                  .filter(
                    ({ tagName }) =>
                      tagName
                        .toLocaleLowerCase()
                        .indexOf(searchValue.toLocaleLowerCase()) > -1
                  )!
                  .map((tag: ITag) => {
                    return (
                      <div
                        className="ProfileCard u-cf"
                        key={tag.tagId}
                        onClick={() => searchSelectedValues(tag.tagName)}
                      >
                        <div className="ProfileCard-avatar">
                          {/*<data.icon />*/}
                        </div>
                        <div className="ProfileCard-details">
                          <div className="ProfileCard-realName">
                            <div className="realname">{tag.tagName}</div>
                          </div>
                        </div>
                      </div>
                    );
                  })
                : ""}
            </div>
            <div className="Typeahead-menu empty-menu">
              <div className="tt-dataset tt-dataset-0">
                <div className="EmptyMessage">{t("Search.NotingFound")}</div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </Fragment>
  );
};
export default observer(SearchHeader);
