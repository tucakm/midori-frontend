import moment from "moment";
import React from "react";
import { AlertCircle } from "react-feather";
import { useTranslation } from "react-i18next";
import { INotification } from "../../../models/notification";
interface Props {
  notification: INotification;
  onClick: () => void;
}
export const NotificationNewReschedule: React.FC<Props> = ({
  notification,
  onClick,
}) => {
  const { t } = useTranslation();

  return (
    <li onClick={onClick}>
      <div className="media">
        <div className="media-body">
          <h6 className="mt-0 txt-danger">
            <span>
              <AlertCircle className="font-danger" />
            </span>
            {t("Notification.RescheduleTitle")}
          </h6>
          <p>
            {t("Notification.RescheduleService", {
              service: notification.orderedService.name,
            })}
          </p>
          <p className="mb-0">
            {t("Notification.RescheduleMessage", {
              date: moment(notification.order.date).format("DD-MM-yyyy"),
              startTime: moment(notification.order.startTime).format("HH"),
              endTime: moment(notification.order.endTime).format("HH"),
            })}
          </p>
        </div>
      </div>
    </li>
  );
};
