import { Moment } from "moment";
import { IService } from "./service";
import { IUser } from "./user";

export interface IOrder {
  id: string;
  service: IService;
  user: IUser;
  date: Moment;
  startTime: Moment;
  endTime: Moment;
  totalPrice: number;
  status: OrderStatus;
  comment: string;
}

export enum OrderStatus {
  Pending = 1,
  Aproved = 2,
  Declined = 3,
  Finished = 4,
  RescheduleNeeded = 5,
  RescheduleSent = 6,
}
