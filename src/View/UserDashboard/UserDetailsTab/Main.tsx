import { Button, Form, Input, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { Col, Row } from "antd/lib/grid";
import TextArea from "antd/lib/input/TextArea";
import { observer } from "mobx-react-lite";
import React from "react";
import { useContext } from "react";
import { country_list } from "../../../app/common/country/country";
import { LoadingComponent } from "../../../app/layout/LoadingComponent";
import { IProfile } from "../../../app/models/profile";
import { RootStoreContext } from "../../../app/stores/rootStore";
import ImageChangeForm from "./ImageChangeForm";


const { Option } = Select;
const Main = () => {
  const rootStore = useContext(RootStoreContext);
  const { loadingProfile, profile, updateProfile, submiting } =
    rootStore.profileStore;
  const { openModal } = rootStore.modalStore;

  const [form] = useForm();

  const handleUpdateProfile = () => {
    form.validateFields().then((fields: IProfile) => {
      var newValues = { ...profile, ...fields };
      updateProfile(newValues);
    });
  };

  if (loadingProfile) return <LoadingComponent />;
  return (
    <div className="container-flud card">
      <div className="edit-profile">
        <Form
          form={form}
          layout="vertical"
          initialValues={profile}
          onFinish={handleUpdateProfile}
        >
          <Row gutter={16}>
            <Col span={8}>
              <div>
                <div>
                  <div className="card-options">
                    <a
                      className="card-options-collapse"
                      href="javascript"
                      data-toggle="card-collapse"
                    >
                      <i className="fe fe-chevron-up"></i>
                    </a>
                    <a
                      className="card-options-remove"
                      href="javascript"
                      data-toggle="card-remove"
                    >
                      <i className="fe fe-x"></i>
                    </a>
                  </div>
                </div>
                <div className="card-body">
                  <div className="row mb-2">
                    <div className="col-auto">
                      <img
                        width={100}
                        onClick={() => {
                          openModal(<ImageChangeForm profile={profile} />)
                        }}
                        alt=""
                        src={profile?.image || "/assets/user.png"}
                      />
                    </div>
                    <div className="col">
                      <h3 className="mb-1">
                        {`${profile.firstName} ${profile.lastName}`}
                      </h3>
                      <p className="mb-4">{profile.job}</p>
                    </div>
                  </div>
                  <Form.Item name="bio" label="Bio" className="form-group">
                    <TextArea className="form-control" rows={5} />
                  </Form.Item>
                  <Form.Item name="email" label="Email address">
                    <Input disabled placeholder="email@email.com" />
                  </Form.Item>
                  <Form.Item name="website" label="Website">
                    <Input placeholder="www.mywebsite.com" />
                  </Form.Item>
                  <Form.Item name="faceBookLink" label="Facebook link">
                    <Input placeholder="Link to your Facebook profile" />
                  </Form.Item>
                  <Form.Item name="twitterLink" label="Twitter link">
                    <Input placeholder="Link to your Twitter profile" />
                  </Form.Item>
                  <Form.Item name="instagramLink" label="Instagram link">
                    <Input placeholder="Link to your Instagram profile" />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col span={14}>
              <div className="card-header">
                <h4 className="card-title mb-0">Edit profile</h4>
                <div className="card-options">
                  <a
                    className="card-options-collapse"
                    href="javascript"
                    data-toggle="card-collapse"
                  >
                    <i className="fe fe-chevron-up"></i>
                  </a>
                  <a
                    className="card-options-remove"
                    href="javascript"
                    data-toggle="card-remove"
                  >
                    <i className="fe fe-x"></i>
                  </a>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-6">
                    <Form.Item name="company" label="Company">
                      <Input placeholder="Company" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-sm-6 col-md-6 ">
                    <Form.Item name="username" label="Username">
                      <Input placeholder="Username" type="text" disabled />
                    </Form.Item>
                  </div>

                  <div className="col-sm-6 col-md-6">
                    <Form.Item name="firstName" label="First name">
                      <Input placeholder="First name" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-sm-6 col-md-6">
                    <Form.Item name="lastName" label="Last name">
                      <Input placeholder="Last name" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-md-6">
                    <Form.Item name="phoneNumber" label="Phone number">
                      <Input placeholder="Phone number" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-md-6">
                    <Form.Item name="address" label="Address">
                      <Input placeholder="Address" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-sm-6 col-md-4">
                    <Form.Item name="city" label="City">
                      <Input placeholder="City" type="text" />
                    </Form.Item>
                  </div>
                  <div className="col-sm-6 col-md-3">
                    <Form.Item name="zipCode" label="Postal code">
                      <Input placeholder="ZIP Code" type="number" />
                    </Form.Item>
                  </div>
                  <div className="col-md-5">
                    <Form.Item name="country" label="Country">
                      <Select placeholder="Country">
                        {country_list.map((value: string, _i) => {
                          return (
                            <Option value={value} key={_i}>
                              {value}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  </div>
                  <div className="col-md-12">
                    <Form.Item name="aboutMe" label="About me">
                      <TextArea rows={5} placeholder="About me" />
                    </Form.Item>
                  </div>
                </div>
              </div>
              <div className="card-footer text-right">
                <Row>
                  <Col offset={12} span={12}>
                    <Button
                      type="primary"
                      block
                      loading={submiting}
                      onClick={handleUpdateProfile}
                    >
                      Save
                    </Button>
                  </Col>
                </Row>
              </div>

            </Col>
          </Row>
        </Form>
      </div>
    </div>
  );
};
export default observer(Main);

