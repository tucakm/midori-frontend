import React, { Fragment, useContext, useEffect } from "react";
import { observer } from "mobx-react-lite";
import {
  Route,
  withRouter,
  RouteComponentProps,
  Switch,
  Redirect,
} from "react-router-dom";
import NotFound from "./NotFound";
import { ToastContainer } from "react-toastify";
import { LoadingComponent } from "./LoadingComponent";
import ModalContainer from "../common/modals/ModalContainer";
import { RootStoreContext } from "../stores/rootStore";
import Header from "../common/header-component/Header";
import Login from "../../View/SignIn/Login";
import Register from "../../View/SignIn/Register";
import ServiceDashboardV2 from "../../View/ServiceProfile/ServiceDashboardV2";
import { HomePageRoutes } from "../../View/Home";
import { ServiceDetailsRoutes } from "../../View/ServiceDetails";
import { UserDashboardRoutes } from "../../View/UserDashboard";
import { ServiceFormRoutes } from "../../View/ServiceForm";
import { ProfileRoutes } from "../../View/Profile";
import { useTranslation } from "react-i18next";

const App: React.FC<RouteComponentProps> = ({ location, match, history }) => {
  const rootStore = useContext(RootStoreContext);
  const { setAppLoaded, token, appLoaded } = rootStore.commonStore;
  const { getUser, user } = rootStore.userStore;
  const { t } = useTranslation();
  useEffect(() => {
    if (token) {
      getUser().finally(() => setAppLoaded());
    } else {
      setAppLoaded();
    }
  }, [token, setAppLoaded, getUser]);
  const notHeaderRoutes = ["/signin", "/signup"];
  if (!appLoaded) return <LoadingComponent content={t("LoadingIndicators.LoadingApp")} />;
  return (
    <Fragment>
      <ModalContainer />
      <ToastContainer position="bottom-right" />
      {!notHeaderRoutes.includes(window.location.pathname) && (
        <Header location={location} history={history} match={match} />
      )}
      <Switch>
        <Redirect from="/home" to="/" />
        <Redirect from="/login" to="/signin" />
        <Route exact path="/" component={HomePageRoutes} />
        <Route exact path={["/signin"]} component={Login}></Route>
        <Route exact path="/signup" component={Register} />
        <Route exact path="/search" component={ServiceDashboardV2} />
        <Route exact path="/service/:id" component={ServiceDetailsRoutes} />
        <Route exact path="/profile/:username" component={ProfileRoutes} />
        {user && (
          <>
            <Route path="/user-dashboard/my" component={UserDashboardRoutes} />
            <Route
              exact
              path={["/createservice", "/manage/:id"]}
              component={ServiceFormRoutes}
            />
          </>
        )}
        <Route component={NotFound} />
      </Switch>
    </Fragment>
  );
};
export default withRouter(observer(App));
