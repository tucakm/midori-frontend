import { Alert, Button, Col, Row, Tag, Typography } from "antd";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { currencyFormater } from "../../../app/common/currency/CurrencyInput";
import { DateDisplay } from "../../../app/common/date-display/DateDisplay";
import { LoadingComponent } from "../../../app/layout/LoadingComponent";
import { OrderStatus } from "../../../app/models/order";
import { RootStoreContext } from "../../../app/stores/rootStore";

interface OrderModalProps {
  orderId: string;
  onStatusChange?: () => void;
}

export const getTagByOrderStatus = (status: OrderStatus): JSX.Element => {
  if (status === OrderStatus.Pending)
    return <Tag color="gold">{OrderStatus[status]}</Tag>;
  else if (status === OrderStatus.Aproved)
    return <Tag color="green">{OrderStatus[status]}</Tag>;
  else if (status === OrderStatus.Finished)
    return <Tag color="#808080">{OrderStatus[status]}</Tag>;
  else if (status === OrderStatus.RescheduleNeeded)
    return <Tag color="red">Reschedule Needed</Tag>;
  else if (status === OrderStatus.RescheduleSent)
    return <Tag color="purple">Reschedule Sent</Tag>;
  else return <Tag>{OrderStatus[status]}</Tag>;
};
const OrderInfo: React.FC<OrderModalProps> = ({ orderId, onStatusChange }) => {
  const { t } = useTranslation();
  const rootStore = useContext(RootStoreContext);
  const { loadOrder, order, changeStatus } = rootStore.orderStore;
  const { closeModal } = rootStore.modalStore;
  useEffect(() => {
    loadOrder(orderId);
  }, [loadOrder, orderId]);
  if (!order) return <LoadingComponent />;

  const handleChangeOfStatus = (status: OrderStatus) => {
    changeStatus(order.id, status).then(() => onStatusChange());
    closeModal();
  };
  const handleAlert = (status: OrderStatus) => {
    if (status === OrderStatus.RescheduleNeeded)
      return (
        <Row style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Alert message={t("OrderInfo.AlertRescheduleText")} type="error" />
          </Col>
        </Row>
      );
    else if (status === OrderStatus.Aproved)
      return (
        <Row style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Alert message={t("OrderInfo.AlertAcceptedText")} type="success" />
          </Col>
        </Row>
      );
    else return null;
  };
  return (
    <div className="container">
      <Row gutter={16}>
        <Col span={24}>
          {handleAlert(order.status)}
          <Row gutter={16}>
            <Col flex="auto">
              <Typography.Title level={3}>{order.id}</Typography.Title>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.ServiceName")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>{order.service.name}</Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.UserOrderedUsername")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>{order.user.username}</Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.UserOrdered")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>{`${order.user.firstName} ${order.user.lastName}`}</Typography.Text>
            </Col>
          </Row>

          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.Date")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>
                <DateDisplay value={order.date} format="DD/MM/YYYY" />
              </Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.StartTime")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>
                <DateDisplay value={order.startTime} format="HH" />
              </Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.EndTime")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>
                <DateDisplay value={order.endTime} format="HH" />
              </Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.TotalPrice")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Text>
                {currencyFormater(order.totalPrice)}
              </Typography.Text>
            </Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.Status")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">{getTagByOrderStatus(order.status)}</Col>
          </Row>
          <Row gutter={16} style={{ marginBottom: 12 }}>
            <Col flex="none">
              <Typography.Text type="secondary">
                {t("OrderInfo.Comment")}:
              </Typography.Text>
            </Col>
            <Col flex="auto">
              <Typography.Paragraph
                ellipsis={{ rows: 3, tooltip: order.comment }}
              >
                {order.comment}
              </Typography.Paragraph>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={4} offset={order.status === OrderStatus.Aproved ||
              order.status === OrderStatus.Pending ? 6 : 8}>
              <Button type="default" block onClick={() => closeModal()}>
                {t("OrderInfo.Close")}
              </Button>
            </Col>
            <Col span={4}>
              <Button
                danger
                block
                onClick={() => handleChangeOfStatus(OrderStatus.Declined)}
              >
                {t("OrderInfo.Decline")}
              </Button>
            </Col>
            {order.status === OrderStatus.Pending && (
              <Col span={4}>
                <Button
                  type="primary"
                  block
                  onClick={() => handleChangeOfStatus(OrderStatus.Aproved)}
                >
                  {t("OrderInfo.Approve")}
                </Button>
              </Col>
            )}
            {order.status === OrderStatus.Aproved && (
              <Col span={4}>
                <Button
                  type="primary"
                  block
                  onClick={() => handleChangeOfStatus(OrderStatus.Finished)}
                >
                  {t("OrderInfo.Finish")}
                </Button>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </div>
  );
};
export default observer(OrderInfo);
