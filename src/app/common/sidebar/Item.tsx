import React, { ReactNode } from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import "./css/style.css";
interface SidebarItemProps {
  to: string;
  icon: ReactNode;
  title: string;
}

export const Item: React.FC<SidebarItemProps> = (props) => {
  return (
    <Fragment>
      <li>
        <Link to={props.to} className={`sidebar-header `}>
          {props.icon}
          <span>{props.title}</span>
        </Link>
      </li>
    </Fragment>
  );
};
