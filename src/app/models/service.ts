import { Moment } from "moment";
import { IReview } from "./review";
import { ITag } from "./tag";
import { IUser } from "./user";

export interface IServicesWrapper {
  services: IService[];
  serviceCount: number;
}

export interface IService {
  id?: string;
  name: string;
  description: string;
  introduction: string;
  price: number;
  createdDate: Moment;
  review: IReview[];
  image?: string;
  images?: string[];
  serviceTags?: ITag[];
  tags?: string[];
  rating: number;
  user: IUser;
  status: ServiceStatus;
  city: string;
}

export enum ServiceStatus {
  Draft = 1,
  Active = 2,
  Inactive = 3,
}
export interface ICity {
  city: string;
}
export interface IServiceFormValues extends Partial<IService> {}
export class ServiceFormValues implements IServiceFormValues {
  id?: string = undefined;
  name: string = "";
  description: string = "";
  introduction: string = "";
  price: number = null;
  tags?: string[] = [];
  status?: ServiceStatus = ServiceStatus.Draft;
  city: string = "";
  constructor(init?: IServiceFormValues) {
    Object.assign(this, init);
    if (init && init.serviceTags && init.serviceTags.length > 0) {
      init.serviceTags.map((item: ITag) => {
        return this.tags.push(item.tagName);
      });
    }
  }
}
