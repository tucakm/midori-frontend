import { observer } from "mobx-react-lite";
import React, { Fragment, useContext } from "react";
import { useTranslation } from "react-i18next";
import { IReview } from "../../../app/models/review";
import { RootStoreContext } from "../../../app/stores/rootStore";
import ReviewItem from "./ReviewItem";

const ReviewLayout: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { service } = rootStore.serviceStore;
  const { t } = useTranslation();
  return (
    <Fragment>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            <div className="blog-single">
              <section className="comment-box">
                <h4>{t("ServiceDetails.Reviews")}</h4>
                <hr />
                <ul>
                  {service!.review?.length > 0 ? (
                    service?.review.map((item: IReview, i: number) => {
                      return (
                        <li key={i}>
                          <ReviewItem review={item} />
                        </li>
                      );
                    })
                  ) : (
                    <div className="text-center">
                      <p>{t("ServiceDetails.NoReviews")}</p>
                    </div>
                  )}
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default observer(ReviewLayout);
