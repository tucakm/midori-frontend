import React from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { observer } from 'mobx-react-lite';

interface IProps{
    fbCallback:(response:any)=>void;
    loading:boolean;
}
const FaceBook:React.FC<IProps> = ({fbCallback,loading}) => {
    return (
     
            <FacebookLogin 
                appId="726252157950811"
                fields="name,email,picture"
                callback={fbCallback}
                render={(renderProps:any)=>(
                    <button disabled={loading} onClick={renderProps.onClick} type="button"  className="btn social-btn btn-fb d-inline-block">
                    <i className="fa fa-facebook"></i>
                    </button>
                )}
            />
       
    )
}
export default observer(FaceBook);
