import React, { useContext } from "react";
import Facebook from "./FaceBook";
import { RootStoreContext } from "../../../app/stores/rootStore";
const SocialLogins = () => {
  const rootStore = useContext(RootStoreContext);
  const { fbLogin, loading } = rootStore.userStore;
  return (
    <div className="social mt-3">
      <div className="form-group btn-showcase d-flex">
        <Facebook loading={loading} fbCallback={fbLogin} />
      </div>
    </div>
  );
};

export default SocialLogins;
