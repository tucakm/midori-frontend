import React, { Fragment, useContext } from "react";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "../../app/stores/rootStore";
import { FORM_ERROR } from "final-form";
import { Form as FinalForm, Field } from "react-final-form";
import { IUserFormValues } from "../../app/models/user";
import {
  combineValidators,
  isRequired,
  createValidator,
  composeValidators,
} from "revalidate";
import { TextInput } from "../../app/common/form/TextInput";
import SocialLogins from "./SocialLogins/SocialLogins";
import { DateInputV2 } from "../../app/common/form/DateInputV2";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from "react-router-dom";
import { ErrorMessage } from "../../app/common/form/ErrorMessage";
import { Button } from "semantic-ui-react";

const isValidEmail = createValidator(
  (message) => (value) => {
    if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return message;
    }
  },
  "Invalid email address"
);

const validate = combineValidators({
  username: isRequired("User Name"),
  firstName: isRequired("First Name"),
  lastName: isRequired("Last Name"),
  email: composeValidators(isRequired("Email"), isValidEmail)(),
  password: isRequired("Password"),
  birthday: isRequired("Date of Birth"),
});

const Register: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { register } = rootStore.userStore;

  return (
    <Fragment>
      <div className="page-wrapper">
        <div className="auth-bg">
          <div className="authentication-box">
            <div className="card mt-4 p-4">
              <h4 className="text-center">Sign up</h4>
              <FinalForm
                onSubmit={(values: IUserFormValues) =>
                  register(values).catch((error) => ({
                    [FORM_ERROR]: error,
                  }))
                }
                validate={validate}
                render={({
                  handleSubmit,
                  submitting,
                  submitError,
                  invalid,
                  pristine,
                  dirtySinceLastSubmit,
                }) => (
                  <form onSubmit={handleSubmit} className="theme-form">
                    <div className="form-row">
                      <div className="col-md-6">
                        <Field
                          name="firstName"
                          component={TextInput}
                          placeholder="John"
                          label="First Name"
                        />
                      </div>
                      <div className="col-md-6">
                        <Field
                          name="lastName"
                          component={TextInput}
                          placeholder="Don"
                          label="Last Name"
                        />
                      </div>
                    </div>
                    <Field
                      name="email"
                      component={TextInput}
                      placeholder="email@host.com"
                      label="Email"
                    />
                    <Field
                      name="username"
                      component={TextInput}
                      placeholder="John Don"
                      label="User Name"
                    />
                    <Field
                      name="password"
                      component={TextInput}
                      placeholder="**********"
                      type="password"
                      label="Password"
                    />
                    <Field
                      name="birthday"
                      label="Date of Birth"
                      component={DateInputV2}
                      placeholder="DD/MM/YYYY"
                      dateFormat="dd/MM/yyyy"
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      maxDate={new Date()}
                    />
                    {submitError && !dirtySinceLastSubmit && (
                      <ErrorMessage error={submitError} />
                    )}

                    <div className="form-row">
                      <div className="col-sm-4">
                        <Button
                          className="btn btn-primary btn-block"
                          color="teal"
                          disabled={
                            (invalid && !dirtySinceLastSubmit) || pristine
                          }
                          loading={submitting}
                          content=" Sign Up"
                          fluid
                        />
                      </div>
                      <div className="col-sm-8">
                        <div className="text-left mt-2 m-l-20">
                          Are you already user?
                          <Link to="/login">Sign in </Link>
                        </div>
                      </div>
                    </div>
                    <div className="form-divider"></div>
                    <SocialLogins />
                  </form>
                )}
              />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default observer(Register);
