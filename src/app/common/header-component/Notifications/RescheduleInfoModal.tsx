import { Button, Col, Row } from "antd";
import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import { INotification } from "../../../models/notification";
import { OrderStatus } from "../../../models/order";
import { RootStoreContext } from "../../../stores/rootStore";

interface Props {
  notification: INotification;
}

export const RescheduleInfoModal: React.FC<Props> = ({ notification }) => {
  const rootStore = useContext(RootStoreContext);
  const { changeStatus } = rootStore.orderStore;
  const { toggleIsRead } = rootStore.notificationStore;
  const { closeModal } = rootStore.modalStore;
  const { t } = useTranslation();
  const handleChangeOfStatus = (status: OrderStatus) => {
    changeStatus(notification.order.id, status)
      .then(() => toggleIsRead(notification))
      .then(() => closeModal());
  };
  return (
    <div>
      <h6 style={{ textAlign: "center" }}>{t("RescheduleInfoModal.Title")}</h6>
      <p style={{ textAlign: "center" }}>
        {t("RescheduleInfoModal.Text", {
          service: notification.orderedService.name,
        })}
      </p>
      <p style={{ textAlign: "center" }}>{notification.message}</p>
      <Row gutter={16}>
        <Col span={4} offset={8}>
          <Button
            danger
            block
            onClick={() => handleChangeOfStatus(OrderStatus.Declined)}
          >
            {t("OrderInfo.Decline")}
          </Button>
        </Col>
        <Col span={4}>
          <Button
            type="primary"
            block
            onClick={() => handleChangeOfStatus(OrderStatus.Aproved)}
          >
            {t("OrderInfo.Approve")}
          </Button>
        </Col>
      </Row>
    </div>
  );
};
