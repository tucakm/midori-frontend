import { Layout, Menu } from "antd";
import { Content } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import React, { useContext, useEffect } from "react";
import {
  UserSwitchOutlined,
  CalendarOutlined,
  DiffOutlined,
  SolutionOutlined,
} from "@ant-design/icons";
import { LoadingComponent } from "../../app/layout/LoadingComponent";
import { RouteComponentProps } from "react-router";
import { Link, Route, Switch } from "react-router-dom";
import { RootStoreContext } from "../../app/stores/rootStore";
import { UserDetaisTab } from "./UserDetailsTab";
import { observer } from "mobx-react-lite";
import NotFound from "../../app/layout/NotFound";
import { ServiceTab } from "./ServicesTab";
import { CalendarTab } from "./CalendarTab";
import { OrderTab } from "./OrdersTab";
import { useTranslation } from "react-i18next";

const UserDashboardLayout: React.FC<RouteComponentProps> = ({
  match,
  location,
}) => {
  const rootStore = useContext(RootStoreContext);
  const { user } = rootStore.userStore;
  const { loadProfile, loadingProfile, isCurrentUser } = rootStore.profileStore;
  const { t } = useTranslation();
  useEffect(() => {
    loadProfile(user.username);
  }, [loadProfile, user.username]);
  if (loadingProfile) return <LoadingComponent />;
  if (!isCurrentUser) return <NotFound />;
  return (
    <Layout style={{ height: "100vh", backgroundColor: "#fffff", paddingTop: "80px" }}>
      <Sider
        className="page-sidebar"
        sidebar-layout="default-sidebar"
        collapsible
        reverseArrow
        style={{
          overflow: "auto",
          height: "100%",
          position: "fixed",
          left: 0,
        }}
        theme="dark"
        width={250}
      >
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[location.pathname + "/services"]}
        >
          <Menu.Item key={match.url + "/services"} icon={<SolutionOutlined />}>
            <span>{t("DashboardLayout.Services")}</span>
            <Link to={match.url + "/services"} />
          </Menu.Item>
          <Menu.Item key="3" icon={<DiffOutlined />}>
            <span>{t("DashboardLayout.SentOrders")}</span>
            <Link to={match.url + "/orders"} />
          </Menu.Item>
          <Menu.Item key={match.url + "/calendar"} icon={<CalendarOutlined />}>
            <span>{t("DashboardLayout.Calendar")}</span>
            <Link to={match.url + "/calendar"} />
          </Menu.Item>
          <Menu.Item
            key={match.url + "/user-details"}
            icon={<UserSwitchOutlined />}
          >
            <span>{t("DashboardLayout.UserDetails")}</span>
            <Link to={match.url + "/user-details"} />
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout style={{ backgroundColor: "#fffff", height: "80vh" }}>
        <Content
          style={{
            margin: "24px 16px 0",
            marginLeft: "260px",
            backgroundColor: "#fffff",
            height: 600,
          }}
        >
          <Switch>
            <Route path={match.path + "/services"} exact>
              <ServiceTab />
            </Route>
            <Route path={match.path + "/calendar"} exact>
              <CalendarTab />
            </Route>
            <Route path={match.path + "/user-details"} exact>
              <UserDetaisTab />
            </Route>
            <Route path={match.path + "/orders"} exact>
              <OrderTab />
            </Route>
          </Switch>
        </Content>
      </Layout>
    </Layout>
  );
};
export default observer(UserDashboardLayout);
