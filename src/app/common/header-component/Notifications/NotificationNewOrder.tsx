import React, { useContext } from "react";
import { ShoppingBag } from "react-feather";
import { useTranslation } from "react-i18next";
import { INotification } from "../../../models/notification";
import { RootStoreContext } from "../../../stores/rootStore";
interface Props {
  notification: INotification;
}
export const NotificationNewOrder: React.FC<Props> = ({ notification }) => {
  const rootStore = useContext(RootStoreContext);
  const { toggleIsRead } = rootStore.notificationStore;
  const { t } = useTranslation();
  return (
    <li onClick={() => toggleIsRead(notification)}>
      <div className="media">
        <div className="media-body">
          <h6 className="mt-0">
            <span>
              <ShoppingBag />
            </span>
            {t("Notification.NewOrder", {
              serviceName: notification.orderedService.name,
            })}
          </h6>
          <p className="mb-0">
            {" "}
            {t("Notification.NewOrderMessage", {
              username: notification.order.user.username,
            })}
          </p>
        </div>
      </div>
    </li>
  );
};
